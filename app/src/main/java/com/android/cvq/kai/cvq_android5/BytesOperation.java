/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

public class BytesOperation {
    public static int bytes2int(byte[] data) {
        if (data.length > 4)
            return -1;

        int i = 0;

        for(int x=data.length-1; x>=0; x--){
//        for (int x = 0; x < data.length; x++) {
            i = i << 8;
            i = i | data[x]&0xff;
        }

        return i;
    }

    public static int bytes2uint(byte[] data) {
        if (data.length > 4)
            return -1;

        return bytes2int(data) & 0xffff;
    }

    public static String bytes2StrUInts(byte[] data) {
        String rtn = "";
        for (byte b : data) {
            rtn += ((int) b & 0xff) + "|";
        }
        return rtn;
    }

    public static byte[] ints2bytes(int[] data, int[] size) {
        if (data.length != size.length) {
            ErrorCatcher.addErrMsg("[int2bytes]: length of data[] and size[] not match. (" + data.length + "," + size.length + ")");
            return null;
        }

        int totalSize = 0;
        for (int i : size) {
            if (i > 4 || i < 1) {
                ErrorCatcher.addErrMsg("[int2bytes]: Size should be 1~4. (" + i + ")");
                return null;
            }
            totalSize += i;
        }

        byte[] rtn = new byte[totalSize];
        int index = 0;
        for (int y = 0; y < data.length; y++) {
            for (int z = 0; z < size[y]; z++) {
                rtn[index] = (byte) ((data[y] >> 8 * z) & 0xff);
                index++;
            }
        }
        return rtn;
    }

    public static byte[] int2bytes(int i) {
        byte[] rtn = new byte[4];
        for (int index = 0; index < 4; index++) {
            i = i >> 8 * index;
            rtn[index] = (byte) (i & 0xff);
        }
        return rtn;
    }

    // byte(-128~127), int(-2^32)~(2^31), uint(0~2^32), binary(0~)

}
