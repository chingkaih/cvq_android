/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import android.content.Context;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.SerialInputOutputManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CVQ_UsbSerial {
    private static UsbSerialPort sPort = null;

    private static MspIdent mspIdent = null;
    private static MspStatus mspStatus = null;
    private static MspRawImu mspRawImu = null;
    private static MspServo mspServo = null;
    private static MspMotor mspMotor = null;
    private static MspRC mspRC = null;
    private static MspPID mspPID = null;
    private static MspRcTuning mspRcTuning = null;
    private static MspMISC mspMISC = null;
    private static MspAttitude mspAttitude = null;
    private static MspAltitude mspAltitude = null;

    private static boolean inited = false;

    private static String bufStr = "";
    private static String bufStr2 = "";
    private static final ExecutorService mExecutor = Executors.newSingleThreadExecutor();

    private static SerialInputOutputManager mSerialIoManager;
    private static final SerialInputOutputManager.Listener mListener =
            new SerialInputOutputManager.Listener() {
                @Override
                public void onRunError(Exception e) {
                    bufStr = "[ERROR]: " + e.getMessage();
                    ErrorCatcher.addErrMsg("[SerialInputOutputManager.Listener]: " + e.toString());
                }

                @Override
                public void onNewData(final byte[] data) {
                    MspCommand mspCmd = new MspCommand(data);
                    Object obj = mspCmd.getAbstractMsp();

//                    LogCatcher.addLogMsg("L=" + data.length + "\n" + BytesOperation.bytes2StrUInts(data));

                    if (obj instanceof MspIdent) {
                        CVQ_UsbSerial.mspIdent = (MspIdent) obj;
                    } else if (obj instanceof MspStatus) {
                        CVQ_UsbSerial.mspStatus = (MspStatus) obj;
                    } else if (obj instanceof MspRawImu) {
                        CVQ_UsbSerial.mspRawImu = (MspRawImu) obj;
                    } else if (obj instanceof MspServo) {
                        CVQ_UsbSerial.mspServo = (MspServo) obj;
                    } else if (obj instanceof MspMotor) {
                        CVQ_UsbSerial.mspMotor = (MspMotor) obj;
                    } else if (obj instanceof MspRC) {
                        CVQ_UsbSerial.mspRC = (MspRC) obj;
                    } else if (obj instanceof MspPID) {
                        CVQ_UsbSerial.mspPID = (MspPID) obj;
                    } else if (obj instanceof MspRcTuning) {
                        CVQ_UsbSerial.mspRcTuning = (MspRcTuning) obj;
                    } else if(obj instanceof MspMISC) {
                        CVQ_UsbSerial.mspMISC = (MspMISC) obj;
                    } else if(obj instanceof MspAttitude){
                        CVQ_UsbSerial.mspAttitude = (MspAttitude) obj;
                    } else if(obj instanceof MspAltitude){
                        CVQ_UsbSerial.mspAltitude = (MspAltitude) obj;
                    } else if (obj == null) {
                        String msg = "[MSG]: ";
                        for(byte b : data){
                            msg += (char)b;
                        }
                        LogCatcher.addLogMsg(msg);
                    } else {
                        ErrorCatcher.addErrMsg("Unimplemented command, " + obj.getClass().toString());
                    }
                }
            };

    private static void init_IOM(Context context){
        if(inited == true)
            return;

//        LogCatcher.addLogMsg("[CVQ_UsbSerial.init_IOM]");

        final List<UsbSerialDriver> drivers = UsbSerialProber.getDefaultProber().findAllDrivers((UsbManager) context.getSystemService(Context.USB_SERVICE));
        final List<UsbSerialPort> result = new ArrayList<UsbSerialPort>();

        for (final UsbSerialDriver driver : drivers) {
            final List<UsbSerialPort> ports = driver.getPorts();
            result.addAll(ports);
        }

        if (result.size() > 0) {
//            LogCatcher.addLogMsg("[" + LogCatcher._FUNC_() + "]: Get Port, size=" + result.size() + ">>>" + result.toString());
            sPort = result.get(0);

        } else {
            ErrorCatcher.addErrMsg("[" + LogCatcher._FUNC_() + "]: result size = 0");
        }
    }

    private static void start_IOM(Context context) {
//        LogCatcher.addLogMsg("[CVQ_UsbSerial.start_IOM]");

        if (mSerialIoManager == null) {
            mSerialIoManager = new SerialInputOutputManager(sPort, mListener);
            mExecutor.submit(mSerialIoManager);
        }
    }

    private static void stop_IOM() {
//        LogCatcher.addLogMsg("[CVQ_UsbSerial.stop_IOM]");
        if (mSerialIoManager != null) {
            mSerialIoManager.stop();
            mSerialIoManager = null;
        }
    }

    public static void pause_IOM() {
//        LogCatcher.addLogMsg("[CVQ_UsbSerial.pause_IOM]");
        if (sPort != null) {
            try {
                sPort.close();
            } catch (IOException e) {
                bufStr = "sPort is null";
            }
            sPort = null;
        }
    }

    public static void resume_IOM(Context context) {
        init_IOM(context);

//        LogCatcher.addLogMsg("[CVQ_UsbSerial.resume_IOM]");
        if (sPort == null) {

        } else {
            final UsbManager usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);

            UsbDeviceConnection connection = usbManager.openDevice(sPort.getDriver().getDevice());
            if (connection == null) {
                bufStr = "cnt null";
                return;
            }

            try {
                sPort.open(connection);
                sPort.setParameters(115200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
            } catch (IOException e) {
                try {
                    sPort.close();
                } catch (IOException e2) {
                    bufStr = "sPort Err";
                }
                sPort = null;
                return;
            }
        }

        stop_IOM();
        start_IOM(context);
    }

    public static String getBufStr() { return bufStr;}
    public static String getBufStr2() {
        return bufStr2;
    }

    public static void write(String msg) {
        if (mSerialIoManager != null) {
            mSerialIoManager.writeAsync(msg.getBytes());
        }
    }

    public static void write(byte[] data) {
        String logMsg = "";

        if (mSerialIoManager != null) {
            mSerialIoManager.writeAsync(data);
        }

        switch (data[4]&0xff){
            case MspCommand.MSP_SET_MISC:
                logMsg = "[Write MSP_SET_MISC]: {";
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspMISC(null)));
                break;
            case MspCommand.MSP_SET_MOTOR:
                logMsg = "[Write MSP_SET_MOTOR]: {";
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspMotor(null)));
                break;
            case MspCommand.MSP_SET_PID:
                logMsg = "[Write MSP_SET_PID]: {";
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspPID(null)));
                break;
            case MspCommand.MSP_SET_RAW_RC:
                logMsg = "[Write MSP_SET_RC]: {";
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRC(null)));
                break;
            case MspCommand.MSP_SET_RC_TUNING:
                logMsg = "[Write MSP_SET_TUNING]: {";
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRcTuning(null)));
                break;
//            case MspCommand.MSP_SET_BOX:
//                break;
//            case MspCommand.MSP_SET_HEAD:
//                break;
//            case MspCommand.MSP_SET_SERVO_CONF:
//                break;
        }

        switch (data[4]&0xff){
            case MspCommand.MSP_SET_MISC:
            case MspCommand.MSP_SET_MOTOR:
            case MspCommand.MSP_SET_PID:
            case MspCommand.MSP_SET_RAW_RC:
            case MspCommand.MSP_SET_RC_TUNING:
                for(byte b : data){
                    logMsg = logMsg + (b&0xff) + ", ";
                }
                logMsg += "}";
                LogCatcher.addLogMsg(logMsg);
                break;
        }
    }
    
    public static MspPID getMspPID() {
        return mspPID;
    }

    public static MspIdent getMspIdent() {
        return mspIdent;
    }

    public static MspStatus getMspStatus() {
        return mspStatus;
    }

    public static MspRawImu getMspRawImu() {
        return mspRawImu;
    }

    public static MspServo getMspServo() {
        return mspServo;
    }

    public static MspMotor getMspMotor() {
        return mspMotor;
    }

    public static MspRC getMspRC() {
        return mspRC;
    }

    public static MspRcTuning getMspRcTuning() {
        return mspRcTuning;
    }

    public static MspMISC getMspMISC(){
        return mspMISC;
    }

    public static MspAltitude getMspAltitude() {return mspAltitude;}

    public static MspAttitude getMspAttitude() {return mspAttitude;}
    
}
