/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

public class FragCtrl_RawRC extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private SeekBar valueBar;
    private Button btnSend;

    private TextView roll;
    private TextView pitch;
    private TextView yaw;
    private TextView throttle;

    private TextView motor_3;
    private TextView motor_5;
    private TextView motor_6;
    private TextView motor_2;

    private RadioGroup ctrlMode;
    private RadioButton ctrlMode_getValue;
    private RadioButton ctrlMode_setup;
    private boolean flag = true;

    private StatusCheck uiUpdateLoop;
    private OnFragmentInteractionListener mListener;

    public FragCtrl_RawRC() {}

    // TODO: Rename and change types and number of parameters
    public static FragCtrl_RawRC newInstance(String param1, String param2) {
        FragCtrl_RawRC fragment = new FragCtrl_RawRC();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_ctrl__raw_rc, container, false);

        valueBar = (SeekBar) view.findViewById(R.id.fcRawRC_ValueBar);
        btnSend = (Button) view.findViewById(R.id.fcRawRC_BtnSend);

        roll = (TextView) view.findViewById(R.id.fcRawRC_Roll);
        roll.setOnTouchListener(new OnTouch2SeekBarListener(valueBar, 1000, 1999, roll));

        pitch = (TextView) view.findViewById(R.id.fcRawRC_Pitch);
        pitch.setOnTouchListener(new OnTouch2SeekBarListener(valueBar, 1000, 1999, pitch));

        yaw = (TextView) view.findViewById(R.id.fcRawRC_Yaw);
        yaw.setOnTouchListener(new OnTouch2SeekBarListener(valueBar, 1000, 1999, yaw));

        throttle = (TextView) view.findViewById(R.id.fcRawRC_Throttle);
        throttle.setOnTouchListener(new OnTouch2SeekBarListener(valueBar, 945, 2199, throttle));

        motor_3 = (TextView) view.findViewById(R.id.fcRawRC_motor3);
        motor_5 = (TextView) view.findViewById(R.id.fcRawRC_motor5);
        motor_6 = (TextView) view.findViewById(R.id.fcRawRC_motor6);
        motor_2 = (TextView) view.findViewById(R.id.fcRawRC_motor2);

        ctrlMode = (RadioGroup) view.findViewById(R.id.fcRawRC_CtrlMode);
        ctrlMode_getValue = (RadioButton) view.findViewById(R.id.fcRawRC_RadioValue);
        ctrlMode_getValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRC(null)));
                flag = true;
            }
        });
        ctrlMode_setup = (RadioButton) view.findViewById(R.id.fcRawRC_RadioSetup);
        ctrlMode_setup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!flag) {
                    roll.setText("1500");
                    yaw.setText("1500");
                    pitch.setText("1500");
                    throttle.setText("1500");
                }else{
                    flag = false;
                }
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ctrlMode.getCheckedRadioButtonId() == R.id.fcRawRC_RadioSetup) {
                    int[] size = {2, 2, 2, 2};

                    int[] intdata = {
                            Integer.parseInt(roll.getText() + ""),
                            Integer.parseInt(pitch.getText() + ""),
                            Integer.parseInt(yaw.getText() + ""),
                            Integer.parseInt(throttle.getText() + "")   };

                    MspSetRawRC mspSetRawRC = new MspSetRawRC(BytesOperation.ints2bytes(intdata, size));
                    CVQ_UsbSerial.write(SendMsp.getBytesData(mspSetRawRC));
                }
            }
        });

        CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRC(null)));

        return view;
    }

    @Override
    public void onPause(){
        super.onPause();
        uiUpdateLoop.stop();
        uiUpdateLoop = null;
    }

    @Override
    public void onResume(){
        super.onResume();
        uiUpdateLoop = new StatusCheck();
        uiUpdateLoop.start();
        uiUpdateLoop.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }

    private class StatusCheck extends CVQ_Async {
        @Override
        void updateValue() {
            MspRC mspRC = CVQ_UsbSerial.getMspRC();
            MspMotor mspMotor = CVQ_UsbSerial.getMspMotor();

            if(ctrlMode.getCheckedRadioButtonId() == R.id.fcRawRC_RadioValue) {
                btnSend.setClickable(false);
                btnSend.setAlpha((float) 0.5);
                if (mspRC != null) {
                    roll.setText(mspRC.getRcRoll() + "");
                    pitch.setText(mspRC.getRcPitch() + "");
                    yaw.setText(mspRC.getRcYaw() + "");
                    throttle.setText(mspRC.getRcThrottle() + "");
                }

                if(mspMotor != null){
                    motor_3.setText(mspMotor.getMotor(0) + "");
                    motor_5.setText(mspMotor.getMotor(1) + "");
                    motor_6.setText(mspMotor.getMotor(2) + "");
                    motor_2.setText(mspMotor.getMotor(3) + "");
                }
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRC(null)));

            }else{
                btnSend.setClickable(true);
                btnSend.setAlpha((float) 1);

                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspMotor(null)));
                if(mspMotor != null){
                    motor_3.setText(mspMotor.getMotor(0) + "");
                    motor_5.setText(mspMotor.getMotor(1) + "");
                    motor_6.setText(mspMotor.getMotor(2) + "");
                    motor_2.setText(mspMotor.getMotor(3) + "");
                }
            }
        }
    };
}
