/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import android.app.Activity;
import android.content.Context;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
//import android.app.Fragment;
//import android.app.FragmentTransaction;
//import android.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

public class FragStatus extends Fragment
                        implements  FragCtrl_Update.OnFragmentInteractionListener,
                                    FragCtrl_DeBug.OnDebugMessageListener,
                                    FragCtrl_RcTuning.OnFragmentInteractionListener,
                                    FragCtrl_Motor.OnFragmentInteractionListener,
                                    FragCtrl_RawRC.OnFragmentInteractionListener,
                                    FragCtrl_MISC.OnFragmentInteractionListener,
                                    FragCtrl_PID.OnFragmentInteractionListener{
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private Spinner spinner;
    private OnFragmentInteractionListener mListener;
    private View cmdView;

    public static String[] items = {
            "Updates", "DeBugs", "RC Tuning", "Set PID", "Set MISC", "Set Raw RC", "Set Motor",
            "--Servo Conf", "--Calibration", "--Set Raw GPS", "--Set Box","--Set Head", "--Set Servo Conf",
            "--Select Setting", "--Reset Conf"};

    public static FragStatus newInstance(String param1, String param2) {
        FragStatus fragment = new FragStatus();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            View view = inflater.inflate(R.layout.fragment_frag_status, container, false);

            spinner = (Spinner) view.findViewById(R.id.msp_cmd_spin);

            ArrayAdapter<String> ctrls = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, items);
            spinner.setAdapter(ctrls);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if (parent.getSelectedItem().toString() == items[0]) {
                        changeFragment(R.id.layout_cmd_frame, new FragCtrl_Update());
                    }
                    else if(parent.getSelectedItem().toString() == items[1]){
                        changeFragment(R.id.layout_cmd_frame, new FragCtrl_DeBug());
                    }
                    else if(parent.getSelectedItem().toString() == items[2]){
                        changeFragment(R.id.layout_cmd_frame, new FragCtrl_RcTuning());
                    }
                    else if(parent.getSelectedItem().toString() == items[3]){
                        changeFragment(R.id.layout_cmd_frame, new FragCtrl_PID());
                    }
                    else if(parent.getSelectedItem().toString() == items[4]){
                        changeFragment(R.id.layout_cmd_frame, new FragCtrl_MISC());
                    }
                    else if(parent.getSelectedItem().toString() == items[5]){
                        changeFragment(R.id.layout_cmd_frame, new FragCtrl_RawRC());
                    }
                    else if(parent.getSelectedItem().toString() == items[6]){
                        changeFragment(R.id.layout_cmd_frame, new FragCtrl_Motor());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            FragCtrl_Update newCtrl = new FragCtrl_Update();
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.add(R.id.layout_cmd_frame, newCtrl).commit();

            return view;

        } catch (Exception e) {
            ErrorCatcher.addErrMsg("[FragStatus]: " + e.getMessage());
        }
        return null;
    }

    private void changeFragment(int container_id, Fragment f){
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        for(Fragment frag: fragmentManager.getFragments()){
            transaction.remove(frag);
        }

        transaction.add(container_id, f);
        transaction.commit();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

    }
}
