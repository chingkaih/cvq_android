/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

public class FragCtrl_MISC extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private SeekBar valueBar;
    private Button btnSend;

    private TextView pwTrigger1;
    private TextView minThro;
    private TextView maxThro;
    private TextView minCmd;
    private TextView fsThro;
    private TextView plogArm;
    private TextView plogLifeTime;
    private TextView conf_magDeclination;
    private TextView conf_vbatscale;
    private TextView conf_vbatlevel_warn1;
    private TextView conf_vbatlevel_warn2;
    private TextView vbatlevel_crit;

    private RadioGroup ctrlMode;
    private RadioButton ctrlMode_getValue;

    private StatusCheck uiUpdateLoop;
    private OnFragmentInteractionListener mListener;

    public FragCtrl_MISC() {}

    // TODO: Rename and change types and number of parameters
    public static FragCtrl_MISC newInstance(String param1, String param2) {
        FragCtrl_MISC fragment = new FragCtrl_MISC();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_frag_ctrl__misc, container, false);

        valueBar = (SeekBar) view.findViewById(R.id.fcMisc_ValueBar);;
        btnSend = (Button) view.findViewById(R.id.fcMisc_BtnSend);

        pwTrigger1  = (TextView) view.findViewById(R.id.fcMisc_PwTrigger1);
        minThro     = (TextView) view.findViewById(R.id.fcMisc_MinThro);
        maxThro     = (TextView) view.findViewById(R.id.fcMisc_MaxThro);
        minCmd      = (TextView) view.findViewById(R.id.fcMisc_MinCmd);
        fsThro      = (TextView) view.findViewById(R.id.fcMisc_FsThro);
        plogArm     = (TextView) view.findViewById(R.id.fcMisc_pArm);
        plogLifeTime            = (TextView) view.findViewById(R.id.fcMisc_pLifetime);
        conf_magDeclination     = (TextView) view.findViewById(R.id.fcMisc_MagDeclip);
        conf_vbatscale          = (TextView) view.findViewById(R.id.fcMisc_VbatScale);
        conf_vbatlevel_warn1    = (TextView) view.findViewById(R.id.fcMisc_VbatWarm1);
        conf_vbatlevel_warn2    = (TextView) view.findViewById(R.id.fcMisc_VbatWarm2);
        vbatlevel_crit          = (TextView) view.findViewById(R.id.fcMisc_VbatCrit);

        ctrlMode    = (RadioGroup) view.findViewById(R.id.fcMisc_CtrlMode);
        ctrlMode_getValue   = (RadioButton) view.findViewById(R.id.fcMisc_RadioValue);
        ctrlMode_getValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspMISC(null)));
            }
        });

        return view;
    }

    @Override
    public void onPause(){
        super.onPause();
        uiUpdateLoop.stop();
        uiUpdateLoop = null;
    }

    @Override
    public void onResume(){
        super.onResume();
        uiUpdateLoop = new StatusCheck();
        uiUpdateLoop.start();
        uiUpdateLoop.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }

    private class StatusCheck extends CVQ_Async{
        @Override
        void updateValue() {
            MspMISC mspMISC = CVQ_UsbSerial.getMspMISC();
            if(ctrlMode.getCheckedRadioButtonId() == R.id.fcMisc_RadioValue) {
                btnSend.setClickable(false);
                btnSend.setAlpha((float) 0.5);
                if (mspMISC != null) {
                    pwTrigger1.setText(mspMISC.getIntPowerTrigger1() + "");
                    minThro.setText(mspMISC.getConf_minthrottle() + "");
                    maxThro.setText(mspMISC.getMAXTHROTTLE() + "");
                    minCmd.setText(mspMISC.getMINCOMMAND() + "");
                    fsThro.setText(mspMISC.getConf_failsafe_throttle() + "");
                    plogArm.setText(mspMISC.getPlog_arm() + "");
                    plogLifeTime.setText(mspMISC.getPlog_lifetime() + "");
                    conf_magDeclination.setText(mspMISC.getConf_mag_declination() + "");
                    conf_vbatscale.setText(mspMISC.getConf_vbatscale() + "");
                    conf_vbatlevel_warn1.setText(mspMISC.getConf_vbatlevel_warn1() + "");
                    conf_vbatlevel_warn2.setText(mspMISC.getConf_vbatlevel_warn2() + "");
                    vbatlevel_crit.setText(mspMISC.getConf_vbatlevel_crit() + "");
                }
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspMISC(null)));
            }else{
//            btnSend.setClickable(true);
//            btnSend.setAlpha((float) 1);
            }
        }
    }
}
