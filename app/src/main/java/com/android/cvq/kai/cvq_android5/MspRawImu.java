/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import java.util.Arrays;

public class MspRawImu extends AbstractMsp {
    private short accx;
    private short accy;
    private short accz;

    private short gyrx;
    private short gyry;
    private short gyrz;

    private short magx;
    private short magy;
    private short magz;

    public MspRawImu(byte[] data) {
        super(MspCommand.MSP_RAW_IMU,data);

        if(data == null){
            this.accx = this.accy = this.accz = this.gyrx = this.gyry = this.gyrz = this.magx = this.magy = this.magz = -1;
            return;
        }

        try {
            this.accx = (short)BytesOperation.bytes2int(Arrays.copyOfRange(data, 0, 2));
            this.accy = (short)BytesOperation.bytes2int(Arrays.copyOfRange(data, 2, 4));
            this.accz = (short)BytesOperation.bytes2int(Arrays.copyOfRange(data, 4, 6));

            this.gyrx = (short)BytesOperation.bytes2int(Arrays.copyOfRange(data, 6, 8));
            this.gyry = (short)BytesOperation.bytes2int(Arrays.copyOfRange(data, 8, 10));
            this.gyrz = (short)BytesOperation.bytes2int(Arrays.copyOfRange(data, 10, 12));

            this.magx = (short)BytesOperation.bytes2int(Arrays.copyOfRange(data, 12, 14));
            this.magy = (short)BytesOperation.bytes2int(Arrays.copyOfRange(data, 14, 16));
            this.magz = (short)BytesOperation.bytes2int(Arrays.copyOfRange(data, 16, 18));
        }catch(Exception e){
            throw e;
        }
    }

    @Override
    public String getJsonStr(){
        String rtn =
                "'rawImu':{" +
                        "'accx':'" + getAccx() +
                        "','accy':'" + getAccy() +
                        "','accz':'" + getAccz() +
                        "','gyrx':'" + getGyrx() +
                        "','gyry':'" + getGyry() +
                        "','gyrz':'" + getGyrz() +
                        "','magx':'" + getMagx() +
                        "','magy':'" + getMagy() +
                        "','magz':'" + getMagz() +
                        "'}";

        return rtn;
    }

    public int getAccx() {
        return this.accx;
    }

    public int getAccy() {
        return this.accy;
    }

    public int getAccz() {
        return this.accz;
    }

    public int getGyrx() {
        return this.gyrx;
    }

    public int getGyry() {
        return this.gyry;
    }

    public int getGyrz() {
        return this.gyrz;
    }

    public int getMagx() {
        return this.magx;
    }

    public int getMagy() {
        return this.magy;
    }

    public int getMagz() {
        return this.magz;
    }
}
