/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import android.os.AsyncTask;

public abstract class CVQ_Async extends AsyncTask<Void, String, String> {
    public static final int updateFrequency = 500;

    private boolean on_run = true;

    abstract void updateValue();

    public void stop(){
        on_run = false;
    }

    public void start(){
        on_run = true;
    }

    @Override
    protected String doInBackground(Void... params) {
        while (on_run) {
            try {
                Thread.sleep(updateFrequency);
            } catch (Exception e) {
                ErrorCatcher.addErrMsg("[" + LogCatcher._FUNC_() + "]: " + e.toString());
            }
            publishProgress();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(String... msg) {
        updateValue();
    }
}
