/* Copyright 23015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

public class CVQ_SocketIO {
    private static Socket cvqSocket = null;
    private static String message = "";

    public static boolean init() {
        try {
            System.out.print("[CVQ_SocketIO] init\n");
            cvqSocket = IO.socket("http://192.168.1.114:5000/msp_cvq_android");
//            cvqSocket = IO.socket("Server Address");
            return true;
        } catch (URISyntaxException e) {
            ErrorCatcher.addErrMsg("CVQ_SocketIO" + e.getMessage());
            return false;
        }
    }

    public static void connect() {
        System.out.print("[CVQ_SocketIO] Connect\n");
        cvqSocket.connect();
        startListen();
    }

    public static void disconnect() {
        System.out.print("[CVQ_SocketIO] disconnect\n");
        cvqSocket.disconnect();
    }

    public static void emit(String event, JSONObject msg) {
        System.out.print("[CVQ_SocketIO] emit\n");
        if (msg != null) {
            cvqSocket.emit(event, msg);
        } else {
            cvqSocket.emit(event);
        }
    }

    public static void emit(String event, String msg) {
        System.out.print("[CVQ_SocketIO] emit\n");
        if (msg != null) {
            cvqSocket.emit(event, msg);
        } else {
            cvqSocket.emit(event);
        }
    }

    private static void startListen() {

        if (cvqSocket != null) {

            System.out.print("[CVQ_SocketIO] startListen\n");

            cvqSocket.on("ping", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    System.out.print("[CVQ_SocketIO] @ping\n");

                    message = "[CVQ_SocketIO]: ";
                    if (args != null && args.length >= 1)
                        message += args[0];
                }
            });

            cvqSocket.on("Req_StatusUpdate", new Emitter.Listener() {
                private JSONObject cvq_status;

                @Override
                public void call(Object... args) {
                    System.out.print("[CVQ_SocketIO] @Req_StatusUpdate\n");
//                    CVQ_UsbSerial.write(SendMsp.getBytesData(new MspMotor(null)));
//                    CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRawImu(null)));

                    String wholeJSON = "{";

                    wholeJSON += "'author':'Ching-Kai Huang'";

                    AbstractMsp msp = CVQ_UsbSerial.getMspIdent();
                    wholeJSON += (msp != null) ? "," + msp.getJsonStr() : "";

                    msp = CVQ_UsbSerial.getMspMISC();
                    wholeJSON += (msp != null) ? "," + msp.getJsonStr() : "";

                    msp = CVQ_UsbSerial.getMspMotor();
                    wholeJSON += (msp != null) ? "," + msp.getJsonStr() : "";

                    msp = CVQ_UsbSerial.getMspPID();
                    wholeJSON += (msp != null) ? "," + msp.getJsonStr() : "";

                    msp = CVQ_UsbSerial.getMspRawImu();
                    wholeJSON += (msp != null) ? "," + msp.getJsonStr() : "";

                    msp = CVQ_UsbSerial.getMspRC();
                    wholeJSON += (msp != null) ? "," + msp.getJsonStr() : "";

                    msp = CVQ_UsbSerial.getMspRcTuning();
                    wholeJSON += (msp != null) ? "," + msp.getJsonStr() : "";

                    msp = CVQ_UsbSerial.getMspServo();
                    wholeJSON += (msp != null) ? "," + msp.getJsonStr() : "";

                    msp = CVQ_UsbSerial.getMspStatus();
                    wholeJSON += (msp != null) ? "," + msp.getJsonStr() : "";

                    msp = CVQ_UsbSerial.getMspAttitude();
                    wholeJSON += (msp != null) ? "," + msp.getJsonStr() : "";

                    msp = CVQ_UsbSerial.getMspAltitude();
                    wholeJSON += (msp != null) ? "," + msp.getJsonStr() : "";

                    wholeJSON += "}";

                    try {
                        cvq_status = new JSONObject(wholeJSON);
                        cvqSocket.emit("StatusUpdate", cvq_status);

                    } catch (JSONException e) {
                        ErrorCatcher.addErrMsg("[CVQ_SocketIO]: " + e.getMessage());
                        cvqSocket.emit("StatusUpdate", new JSONObject());
                    }
                }
            });

            cvqSocket.on("Set_RcTuning", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    try {
                        JSONObject parameters = new JSONObject((String) args[0]);
                        int[] size = {1, 1, 1, 1, 1, 1, 1};
                        int[] intData = {
                                parameters.getInt("rc_rate"),
                                parameters.getInt("rc_expo"),
                                parameters.getInt("rollpitch_rate"),
                                parameters.getInt("yaw_rate"),
                                parameters.getInt("dynThroPID"),
                                parameters.getInt("thro_mid"),
                                parameters.getInt("thro_expo")
                        };

                        MspSetRcTuning mspSetRcTuning = new MspSetRcTuning(BytesOperation.ints2bytes(intData, size));
                        CVQ_UsbSerial.write(SendMsp.getBytesData(mspSetRcTuning));

                    } catch (JSONException e) {
                        message = "[intData_ERROR]" + e.getMessage();
                    }
                }
            });

            cvqSocket.on("Set_RawRC", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    System.out.print("[CVQ_SocketIO] @Set_RawRC:: " + args[0] + "\n");

                    try {
                        JSONObject parameters = new JSONObject((String) args[0]);
//                        JSONObject parameters = new JSONObject("{'roll':'1501','pitch':'1502','yaw':'1503','throttle':'1504'}");

                        int[] intData = {
                                parameters.getInt("roll"),
                                parameters.getInt("pitch"),
                                parameters.getInt("yaw"),
                                parameters.getInt("throttle"),
                        };

                        int[] size = {2, 2, 2, 2};
                        MspSetRawRC mspSetRawRC = new MspSetRawRC(BytesOperation.ints2bytes(intData, size));
                        CVQ_UsbSerial.write(SendMsp.getBytesData(mspSetRawRC));

                    } catch (JSONException e) {
                        message = "[intData_ERROR]" + e.getMessage();
                    }
                }
            });

            cvqSocket.on("Set_Motors", new Emitter.Listener(){
                @Override
                public void call(Object... args) {
                    System.out.print("[CVQ_SocketIO] @Set_Motors\n");
                    try{
                        JSONObject parameters = new JSONObject((String) args[0]);
                        int[] size = {  2, 2, 2, 2, 2, 2, 2, 2,
                                2, 2, 2, 2, 2, 2, 2, 2  };

                        int[] intdata = {
                            Integer.parseInt(parameters.get("m2") + ""),
                            Integer.parseInt(parameters.get("m5") + ""),
                            Integer.parseInt(parameters.get("m6") + ""),
                            Integer.parseInt(parameters.get("m3") + ""),
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

                        MspSetMotor mspSetMotor = new MspSetMotor(BytesOperation.ints2bytes(intdata, size));
                        CVQ_UsbSerial.write(SendMsp.getBytesData(mspSetMotor));

                    }catch (Exception e){
                        LogCatcher.addLogMsg("[CVQ_SocketIO]: " + args[0].toString());
                        ErrorCatcher.addErrMsg("[CVQ_SocketIO]: " + e.getMessage());
                    }
                }
            });

            cvqSocket.on("Set_PID", new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    System.out.print("[CVQ_SocketIO] @Set_PID\n");

                    try {
                        int countDown = 3;
                        JSONObject parameters = new JSONObject((String) args[0]);

                        while (CVQ_UsbSerial.getMspPID() == null) {
                            countDown--;
                            if (countDown < 0)
                                return;
                            CVQ_UsbSerial.write(SendMsp.getBytesData(new MspPID(null)));
                            Thread.sleep(1000);
                        }

                        MspPID mspPID = CVQ_UsbSerial.getMspPID();
                        int[] size = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                                1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

                        int[] intData = {
                                Integer.parseInt(parameters.getJSONObject("roll").getString("P")),
                                Integer.parseInt(parameters.getJSONObject("roll").getString("I")),
                                Integer.parseInt(parameters.getJSONObject("roll").getString("D")),
                                Integer.parseInt(parameters.getJSONObject("pitch").getString("P")),
                                Integer.parseInt(parameters.getJSONObject("pitch").getString("I")),
                                Integer.parseInt(parameters.getJSONObject("pitch").getString("D")),
                                Integer.parseInt(parameters.getJSONObject("yaw").getString("P")),
                                Integer.parseInt(parameters.getJSONObject("yaw").getString("I")),
                                Integer.parseInt(parameters.getJSONObject("yaw").getString("D")),

                                mspPID.getValuePID(MspPID.pidItems[3], MspPID.P),
                                mspPID.getValuePID(MspPID.pidItems[3], MspPID.I),
                                mspPID.getValuePID(MspPID.pidItems[3], MspPID.D),
                                mspPID.getValuePID(MspPID.pidItems[4], MspPID.P),
                                mspPID.getValuePID(MspPID.pidItems[4], MspPID.I),
                                mspPID.getValuePID(MspPID.pidItems[4], MspPID.D),
                                mspPID.getValuePID(MspPID.pidItems[5], MspPID.P),
                                mspPID.getValuePID(MspPID.pidItems[5], MspPID.I),
                                mspPID.getValuePID(MspPID.pidItems[5], MspPID.D),
                                mspPID.getValuePID(MspPID.pidItems[6], MspPID.P),
                                mspPID.getValuePID(MspPID.pidItems[6], MspPID.I),
                                mspPID.getValuePID(MspPID.pidItems[6], MspPID.D),
                                mspPID.getValuePID(MspPID.pidItems[7], MspPID.P),
                                mspPID.getValuePID(MspPID.pidItems[7], MspPID.I),
                                mspPID.getValuePID(MspPID.pidItems[7], MspPID.D),
                                mspPID.getValuePID(MspPID.pidItems[8], MspPID.P),
                                mspPID.getValuePID(MspPID.pidItems[8], MspPID.I),
                                mspPID.getValuePID(MspPID.pidItems[8], MspPID.D),
                                mspPID.getValuePID(MspPID.pidItems[9], MspPID.P),
                                mspPID.getValuePID(MspPID.pidItems[9], MspPID.I),
                                mspPID.getValuePID(MspPID.pidItems[9], MspPID.D)
                        };

                        MspSetPID mspSetPID = new MspSetPID(BytesOperation.ints2bytes(intData, size));
                        CVQ_UsbSerial.write(SendMsp.getBytesData(mspSetPID));

                    } catch (Exception e) {
                        message = "[intData_ERROR]" + e.getMessage();
                    }
                }
            });
        }
    }

    public static boolean isConnected() {
        if (cvqSocket != null && cvqSocket.connected()) {
//            System.out.print("[CVQ_SocketIO] isConnected\n");
            return true;
        } else {
//            System.out.print("[CVQ_SocketIO] isNotConnected\n");
            return false;
        }
    }

    public static String getMsg() {
        String rtn = message;
        //message = "";
        return rtn;
    }

}
