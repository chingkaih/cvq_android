/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FragCtrl_Update extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    OnHeadlineSelectedListener mCallback;
    private OnFragmentInteractionListener mListener;

    private Button btnIdent;
    private Button btnStatus;
    private Button btnRawIMU;
    private Button btnMotor;
    private Button btnRC;
    private Button btnPID;
//    private Button btnServo;
//    private Button btnAnalog;
//    private Button btnBox;
//    private Button btnMISC;
//    private Button btnMotorPins;
//    private Button btnBoxNames;
//    private Button btnPIDNames;
    private Button btnAll;

    public static FragCtrl_Update newInstance(String param1, String param2) {
        FragCtrl_Update fragment = new FragCtrl_Update();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_ctrl__update, container, false);

        btnStatus = (Button) view.findViewById(R.id.ctrlStatus);
        btnIdent = (Button) view.findViewById(R.id.ctrlIdent);
        btnRawIMU = (Button) view.findViewById(R.id.ctrlRawIMU);
        btnMotor = (Button) view.findViewById(R.id.ctrlMotor);
        btnRC = (Button) view.findViewById(R.id.ctrlRC);
        btnPID = (Button) view.findViewById(R.id.ctrlPID);
//        btnServo = (Button) view.findViewById(R.id.ctrlServo);
//        btnAnalog = (Button) view.findViewById(R.id.ctrlAnalog);
//        btnBox = (Button) view.findViewById(R.id.ctrlBox);
//        btnMISC = (Button) view.findViewById(R.id.ctrlMisc);
//        btnMotorPins = (Button) view.findViewById(R.id.ctrlMotorPins);
//        btnBoxNames = (Button) view.findViewById(R.id.ctrlBoxNames);
//        btnPIDNames = (Button) view.findViewById(R.id.ctrlPIDNames);
        btnAll = (Button) view.findViewById(R.id.ctrlAll);

        //------------------------------

        btnStatus.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspStatus(null)));
            }
        });

        btnIdent.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspIdent(null)));
            }
        });

        btnRawIMU.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRawImu(null)));
            }
        });

        btnMotor.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspMotor(null)));
            }
        });

        btnRC.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRC(null)));
            }
        });

        btnPID.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspPID(null)));
            }
        });

//        btnServo.setOnClickListener(new Button.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspServo(null)));
//            }
//        });

//        btnAnalog.setOnClickListener(new Button.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspAnalog(null)));
//            }
//        });

//        btnBox.setOnClickListener(new Button.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspBox(null)));
//            }
//        });

//        btnMISC.setOnClickListener(new Button.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspMISC(null)));
//            }
//        });

//        btnMotorPins.setOnClickListener(new Button.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspMotorPin(null)));
//            }
//        });

//        btnBoxNames.setOnClickListener(new Button.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspBoxNames(null)));
//            }
//        });

//        btnPIDNames.setOnClickListener(new Button.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CVQ_UsbSerial.write(sendMsp.getBytesData(new MspPIDNames(null)));
//            }
//        });

        btnAll.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] mspCmdIds = {MspCommand.MSP_PID, MspCommand.MSP_RC, MspCommand.MSP_MOTOR, MspCommand.MSP_IDENT, MspCommand.MSP_STATUS};
                CVQ_UsbSerial.write(SendMsp.cmdIds2bytes(mspCmdIds));
            }
        });

        return view;
    }

    public interface OnHeadlineSelectedListener {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface OnFragmentInteractionListener {

    }
}
