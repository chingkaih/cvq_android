/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import java.util.Arrays;

public class MspStatus extends AbstractMsp {
    private int cycleTime;                  // uint 16
    private int i2c_errors_count;           // uint 16
    private int sensor;                     // uint 16
    private long flag;                       // uint 32
    private int global_conf_currentSet;     // uint  8

    public MspStatus(byte[] data){
        super(MspCommand.MSP_STATUS, data);

        if(data == null){
            cycleTime = i2c_errors_count = sensor = global_conf_currentSet = -1;
            flag = -1;

            return;
        }

        try {
            this.cycleTime = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 0, 2));
            this.i2c_errors_count = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 2, 4));
            this.sensor = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 4, 6));
            this.flag = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 6, 10));
            this.global_conf_currentSet = data[10] & 0xff;
        }catch(Exception e){
            throw e;
        }
    }

    public int getCycleTime(){
        return this.cycleTime;
    }

    public int getI2c_errors_count(){
        return this.i2c_errors_count;
    }

    public int getSensor(){
        return this.sensor;
    }

    public long getFlag(){
        return this.flag;
    }

    public int getGlobal_conf_currentSet(){
        return this.global_conf_currentSet;
    }

    @Override
    public String getJsonStr(){
        String rtn =
                "'status':{" +
                        "'cycle_time':'" + getCycleTime() +
                        "','i2c_err_count':'" + getI2c_errors_count() +
                        "','Sensor':" + getSensor() +
                        "','flag':'" + getFlag() +
                        "','currentSet':" + getGlobal_conf_currentSet() +
                        "'}";

        return rtn;
    }
}
