/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import java.util.Arrays;

public class MspIdent extends AbstractMsp {
    private int ver         = -1;   // uint 8
    private int multiType   = -1;   // uint 8
    private int mspVersion  = -1;   // uint 8
    private long capability = -1;   // uint 32

    public static final String[] types = {"unknown", "TRI", "QUADP", "QUADX", "BI", "GIMBAL", "Y6",
            "HEX6", "FLYING_WING", "Y4", "HEX6X", "OCTOX8", "OCTOFLATX", "OCTOFLATP", "AIRPLANE",
            "HELI_120_CCPM", "HELI_90_DEG", "VTAIL4", "HEX6H", "PPM_TO_SERVO", "DUALCOPTER",
            "SINGLECOPTER"};

    public MspIdent(byte[] data){
        super(MspCommand.MSP_IDENT, data);

        if(data == null) {
            return;
        }

        try {
            this.ver = (int) data[0] & 0xff;
            this.multiType = (int) data[1] & 0xff;
            this.mspVersion = (int) data[2] & 0xff;
            this.capability = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 3, 7));
        }catch(Exception e){
            throw e;
        }
    }

    @Override
    public String getJsonStr(){
        String rtn =
                "'ident':{" +
                        "'ver':'" + getVersion() +
                        "','type':'" + getMultiType() +
                        "','msp_ver':'" + getMspVersion() +
                        "','capbility':'" + getCapability() +
                "'}";

        return rtn;
    }

    public int getVersion(){ return this.ver;}

    public int getMultiType(){
        return this.multiType;
    }

    public int getMspVersion(){
        return this.mspVersion;
    }

    public long getCapability(){
        return this.capability;
    }
}
