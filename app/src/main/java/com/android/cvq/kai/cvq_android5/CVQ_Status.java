/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

//import android.app.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.PowerManager;
import android.support.v4.app.Fragment;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class CVQ_Status extends ActionBarActivity
        implements FragStatus.OnFragmentInteractionListener,
        FragCtrl_DeBug.OnDebugMessageListener,
        FragCtrl_Update.OnFragmentInteractionListener,
        FragCtrl_RcTuning.OnFragmentInteractionListener,
        FragCtrl_Motor.OnFragmentInteractionListener,
        FragCtrl_RawRC.OnFragmentInteractionListener,
        FragCtrl_MISC.OnFragmentInteractionListener,
        FragCtrl_PID.OnFragmentInteractionListener,
        FragInitProcess.OnFragmentInteractionListener{

    private StatusCheck uiUpdateLoop;

    private FragStatus fragStatus = new FragStatus();
    private FragmentTransaction transaction;

    private Button btn;

    private long s_time;
    private long c_time;

    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;

    private boolean inited = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cvq__status);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        s_time = System.currentTimeMillis();
        c_time = s_time;

        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "[CVQ_KeepWake]");

        btn = (Button) findViewById(R.id.btn_send);
        btn.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                CVQ_SocketIO.emit("ping", "message from Android");
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        wakeLock.acquire();
        uiUpdateLoop = new StatusCheck();
        uiUpdateLoop.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cvq__status, menu);

        return true;
    }

    //------------------------------------------
    @Override
    protected void onPause() {
        super.onPause();

        CVQ_UsbSerial.pause_IOM();
        CVQ_SocketIO.disconnect();

        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        CVQ_UsbSerial.resume_IOM(this.getApplicationContext());

        CVQ_SocketIO.init();
        CVQ_SocketIO.connect();
    }

    @Override
    protected void onStop(){
        super.onStop();
        wakeLock.release();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(inited) {
            int id = item.getItemId();

            if (id == R.id.menu_status) {
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                for (Fragment f : getSupportFragmentManager().getFragments()) {
                    if (f != null) {
                        transaction.remove(f);
                    }
                }
                transaction.commit();

                return true;

            } else if (id == R.id.menu_cmd) {
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.replace(R.id.main_act, fragStatus);
                transaction.commit();

                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private class StatusCheck extends AsyncTask<Void, String, String> {
        int nextUpdateIndex = 0;
        AbstractMsp[] reqUpdateList = {
                new MspRawImu(null),
                new MspMotor(null)
        };

        TextView id_ver = (TextView) findViewById(R.id.id_ver);
        TextView id_type = (TextView) findViewById(R.id.id_type);
        TextView id_msp_ver = (TextView) findViewById(R.id.id_msp_ver);
        TextView id_cap = (TextView) findViewById(R.id.id_cap);

        TextView stat_cytime = (TextView) findViewById(R.id.stat_cytime);
        TextView stat_flag = (TextView) findViewById(R.id.stat_flag);
        TextView stat_g_conf = (TextView) findViewById(R.id.stat_g_conf);
        TextView stat_i2c_err_c = (TextView) findViewById(R.id.stat_i2c_err_c);
        TextView stat_sensor = (TextView) findViewById(R.id.stat_sensor);

        TextView[] motors = {
                (TextView) findViewById(R.id.motor_3),
                (TextView) findViewById(R.id.motor_5),
                (TextView) findViewById(R.id.motor_6),
                (TextView) findViewById(R.id.motor_2)
        };

        TextView rcPitch = (TextView) findViewById(R.id.rc_pitch);
        TextView rcRoll = (TextView) findViewById(R.id.rc_roll);
        TextView rcThrottle = (TextView) findViewById(R.id.rc_throttle);
        TextView rcYaw = (TextView) findViewById(R.id.rc_yaw);

        TextView accX = (TextView) findViewById(R.id.accx);
        TextView accY = (TextView) findViewById(R.id.accy);
        TextView accZ = (TextView) findViewById(R.id.accz);

        TextView gyrX = (TextView) findViewById(R.id.gyrx);
        TextView gyrY = (TextView) findViewById(R.id.gyry);
        TextView gyrZ = (TextView) findViewById(R.id.gyrz);

        TextView magX = (TextView) findViewById(R.id.magx);
        TextView magY = (TextView) findViewById(R.id.magy);
        TextView magZ = (TextView) findViewById(R.id.magz);

        TextView pitch_p = (TextView) findViewById(R.id.pitch_p);
        TextView pitch_i = (TextView) findViewById(R.id.pitch_i);
        TextView pitch_d = (TextView) findViewById(R.id.pitch_d);

        TextView yaw_p = (TextView) findViewById(R.id.yaw_p);
        TextView yaw_i = (TextView) findViewById(R.id.yaw_i);
        TextView yaw_d = (TextView) findViewById(R.id.yaw_d);

        TextView roll_p = (TextView) findViewById(R.id.roll_p);
        TextView roll_i = (TextView) findViewById(R.id.roll_i);
        TextView roll_d = (TextView) findViewById(R.id.roll_d);

        RadioButton onlineSignal = (RadioButton) findViewById(R.id.onlineSingnal);

        @Override
        protected String doInBackground(Void... params) {
            int updateMSec = 300;
            try {
                FragInitProcess fragInitProcess = new FragInitProcess();
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.replace(R.id.main_act, fragInitProcess);
                transaction.commit();

                fragInitProcess.setInitProgress(8);
                fragInitProcess.setInitText("Starting");

                Thread.sleep(1500);

                fragInitProcess.setInitProgress(20);
                fragInitProcess.setInitText("Connect to MultiWii");
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspStatus(null)));
                while(CVQ_UsbSerial.getMspStatus() == null){Thread.sleep(updateMSec);}

                fragInitProcess.setInitProgress(27);
                fragInitProcess.setInitText("Get Identity");
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspIdent(null)));
                while(CVQ_UsbSerial.getMspIdent() == null){Thread.sleep(updateMSec);}

                fragInitProcess.setInitProgress(34);
                fragInitProcess.setInitText("Get RawImu");
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRawImu(null)));
                while(CVQ_UsbSerial.getMspRawImu() == null){Thread.sleep(updateMSec);}

                fragInitProcess.setInitProgress(45);
                fragInitProcess.setInitText("Init Motors");
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspMotor(null)));
                while(CVQ_UsbSerial.getMspMotor() == null){Thread.sleep(updateMSec);}

                fragInitProcess.setInitProgress(53);
                fragInitProcess.setInitText("Get RC Value");
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRC(null)));
                while(CVQ_UsbSerial.getMspRC() == null){Thread.sleep(updateMSec);}

                fragInitProcess.setInitProgress(69);
                fragInitProcess.setInitText("Get PID");
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspPID(null)));
                while(CVQ_UsbSerial.getMspPID() == null){Thread.sleep(updateMSec);}

                fragInitProcess.setInitProgress(75);
                fragInitProcess.setInitText("Get RC Tuning");
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRcTuning(null)));
                while(CVQ_UsbSerial.getMspRcTuning() == null){Thread.sleep(updateMSec);}

                fragInitProcess.setInitProgress(89);
                fragInitProcess.setInitText("& Others...");
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspMISC(null)));
                while(CVQ_UsbSerial.getMspMISC() == null){Thread.sleep(updateMSec);}

                fragInitProcess.setInitProgress(100);
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                for (Fragment f : getSupportFragmentManager().getFragments()) {
                    if (f != null) {
                        transaction.remove(f);
                    }
                }
                transaction.commit();
                inited = true;

            } catch (InterruptedException e) {
                ErrorCatcher.addErrMsg("[" + LogCatcher._FUNC_() + "]: " + e.toString());
            }

            while (true) {
                try {
                    Thread.sleep(updateMSec);

                } catch (Exception e) {
                    ErrorCatcher.addErrMsg("[" + LogCatcher._FUNC_() + "]: " + e.toString());
                }

                publishProgress();
            }
        }

        @Override
        protected void onProgressUpdate(String... msg) {

            c_time = System.currentTimeMillis();
            if (c_time - s_time > 500) {
                s_time = c_time;

                if(nextUpdateIndex == reqUpdateList.length)
                    nextUpdateIndex = 0;
                CVQ_UsbSerial.write(SendMsp.getBytesData(reqUpdateList[nextUpdateIndex++]));
            } else {
                return;
            }

            MspIdent mspIdent = CVQ_UsbSerial.getMspIdent();
            if (mspIdent != null) {
                this.id_ver.setText(mspIdent.getVersion() + "");
                this.id_type.setText(MspIdent.types[mspIdent.getMultiType()]);
                this.id_msp_ver.setText(mspIdent.getMspVersion() + "");
                this.id_cap.setText(mspIdent.getCapability() + "");
            }

            MspStatus mspStatus = CVQ_UsbSerial.getMspStatus();
            if (mspStatus != null) {
                this.stat_cytime.setText(mspStatus.getCycleTime() + "");
                this.stat_flag.setText(mspStatus.getFlag() + "");
                this.stat_g_conf.setText(mspStatus.getGlobal_conf_currentSet() + "");
                this.stat_i2c_err_c.setText(mspStatus.getI2c_errors_count() + "");
                this.stat_sensor.setText(mspStatus.getSensor() + "");
            }

            MspMotor mspMotor = CVQ_UsbSerial.getMspMotor();
            if (mspMotor != null) {
                for (int x = 0; x < 4; x++) {
                    this.motors[x].setText(mspMotor.getMotor(x) + "");
                }
            }

            MspRC mspRC = CVQ_UsbSerial.getMspRC();
            if (mspRC != null) {
                this.rcPitch.setText(mspRC.getRcPitch() + "");
                this.rcRoll.setText(mspRC.getRcRoll() + "");
                this.rcThrottle.setText(mspRC.getRcThrottle() + "");
                this.rcYaw.setText(mspRC.getRcYaw() + "");
            }

            MspRawImu mspRawImu = CVQ_UsbSerial.getMspRawImu();
            if (mspRawImu != null) {
                this.accX.setText(mspRawImu.getAccx() + "");
                this.accY.setText(mspRawImu.getAccy() + "");
                this.accZ.setText(mspRawImu.getAccz() + "");

                this.gyrX.setText(mspRawImu.getGyrx() + "");
                this.gyrY.setText(mspRawImu.getGyry() + "");
                this.gyrZ.setText(mspRawImu.getGyrz() + "");

                this.magX.setText(mspRawImu.getMagx() + "");
                this.magY.setText(mspRawImu.getMagy() + "");
                this.magZ.setText(mspRawImu.getMagz() + "");
            }

            MspPID mspPID = CVQ_UsbSerial.getMspPID();
            if (mspPID != null) {
                this.roll_p.setText(mspPID.getValuePID(MspPID.roll, MspPID.P) + "");
                this.pitch_p.setText(mspPID.getValuePID(MspPID.pitch, MspPID.P) + "");
                this.yaw_p.setText(mspPID.getValuePID(MspPID.yaw, MspPID.P) + "");

                this.roll_i.setText(mspPID.getValuePID(MspPID.roll, MspPID.I) + "");
                this.pitch_i.setText(mspPID.getValuePID(MspPID.pitch, MspPID.I) + "");
                this.yaw_i.setText(mspPID.getValuePID(MspPID.yaw, MspPID.I) + "");

                this.roll_d.setText(mspPID.getValuePID(MspPID.roll, MspPID.D) + "");
                this.pitch_d.setText(mspPID.getValuePID(MspPID.pitch, MspPID.D) + "");
                this.yaw_d.setText(mspPID.getValuePID(MspPID.yaw, MspPID.D) + "");
            }

            if(CVQ_SocketIO.isConnected()){
                onlineSignal.setText("Connected");
                onlineSignal.setChecked(true);
                onlineSignal.setTextColor(Color.GREEN);
            }else{
                onlineSignal.setText("Disconnected");
                onlineSignal.setChecked(false);
                onlineSignal.setTextColor(Color.RED);
            }

            String socketioMsg = CVQ_SocketIO.getMsg();
            if(socketioMsg.length() > 0)
                this.stat_g_conf.setText(socketioMsg);

//            this.id_type.setText(CVQ_UsbSerial.getBufStr2());
//            this.id_ver.setText(CVQ_UsbSerial.getBufStr());

//            MspServo mspServo = CVQ_UsbSerial.getMspServo();
//            if (mspServo != null) {
//                //
//            } else {

//            }
        }
    }
}
