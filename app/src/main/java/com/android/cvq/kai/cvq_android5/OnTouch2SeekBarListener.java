/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import android.view.MotionEvent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class OnTouch2SeekBarListener implements View.OnTouchListener {
    public final static float range_max = 250;
    public final static float range_min = -250;

    private SeekBar valueBar;
    private int max;
    private int min;
    private  TextView textView;


    public OnTouch2SeekBarListener(SeekBar seekBar, int min, int max, TextView textView){
        super();
        this.valueBar = seekBar;
        this.max = max;
        this.min = min;
        this.textView = textView;

        if(valueBar != null)
            valueBar.setMax(max-min);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            if(valueBar != null)
                valueBar.setVisibility(View.VISIBLE);
        }else if(event.getAction() == MotionEvent.ACTION_UP) {
            if(valueBar != null)
                valueBar.setVisibility(View.INVISIBLE);
        }
        if(event.getAction() == MotionEvent.ACTION_MOVE){
            float axis_y = ((-1)*event.getY() - range_min) * (max-min)/(range_max-range_min);
            axis_y += min;

            if(axis_y > max) {
                axis_y = max;
            }else if(axis_y < min){
                axis_y = min;
            }

            if(valueBar != null)
                valueBar.setProgress((int) axis_y - min);

            if(textView != null)
                textView.setText((int)axis_y + "");
        }

        return false;
    }
}
