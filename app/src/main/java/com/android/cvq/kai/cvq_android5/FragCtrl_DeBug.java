/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class FragCtrl_DeBug extends Fragment {
    public FragCtrl_DeBug() {}

    // TODO: Rename parameter arguments, choose names that match
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button btnAccCalib;
    private Button btnMagCalib;
    private TextView debugText;

    private StatusCheck uiUpdateLoop;
    private OnDebugMessageListener debugMessageListener;

    public static FragCtrl_DeBug newInstance(String param1, String param2) {
        FragCtrl_DeBug fragment = new FragCtrl_DeBug();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_ctrl__de_bug, container, false);
        debugText = (TextView) view.findViewById(R.id.debugText);
        btnAccCalib = (Button) view.findViewById(R.id.ctrlDebugAccCalibration);
        btnAccCalib.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspAccCalibration()));
            }
        });
        btnMagCalib = (Button) view.findViewById(R.id.ctrlDebugMagCalibration);
        btnMagCalib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspMagCalivration()));
            }
        });

        return view;
    }

    @Override
    public void onPause(){
        super.onPause();
        uiUpdateLoop.stop();
        uiUpdateLoop = null;
    }

    @Override
    public void onResume(){
        super.onResume();
        uiUpdateLoop = new StatusCheck();
        uiUpdateLoop.start();
        uiUpdateLoop.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
//            debugMessageListener = (OnDebugMessageListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        debugMessageListener = null;
    }

    public interface OnDebugMessageListener {
        // TODO: Update argument type and name
//        public void addDebugMsg(String debugMsg);
    }

    private class StatusCheck extends CVQ_Async {

        @Override
        void updateValue() {
            String tmp = ErrorCatcher.getErrMsg();
            if(tmp.length() > 0) {
                debugText.setText(tmp + "\n" + debugText.getText());
                ErrorCatcher.clearErrMsg();
            }

            tmp = LogCatcher.getLogMsg();
            if(tmp.length() > 0) {
                debugText.setText(tmp + "\n" + debugText.getText());
                LogCatcher.clearLog();
            }
        }
    }

}
