/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import java.util.Arrays;

public class MspMISC extends AbstractMsp {
    private int intPowerTrigger1        = -1;     // uint 16
    private int conf_minthrottle        = -1;     // uint 16
    private int MAXTHROTTLE             = -1;     // uint 16
    private int MINCOMMAND              = -1;     // uint 16
    private int conf_failsafe_throttle  = -1;     // uint 16
    private int plog_arm                = -1;     // uint 16
    private long plog_lifetime          = -1;     // uint 32
    private int conf_mag_declination    = -1;     // uint 16
    private int conf_vbatscale          = -1;     // uint 8
    private int conf_vbatlevel_warn1    = -1;     // uint 8
    private int conf_vbatlevel_warn2    = -1;     // uint 8
    private int conf_vbatlevel_crit     = -1;     // uint 8

    public int getIntPowerTrigger1(){return this.intPowerTrigger1;}
    public int getConf_minthrottle(){return this.conf_minthrottle;}
    public int getMAXTHROTTLE(){return this.MAXTHROTTLE;}
    public int getMINCOMMAND(){return this.MINCOMMAND;}
    public int getConf_failsafe_throttle(){return this.conf_failsafe_throttle;}
    public int getPlog_arm(){return this.plog_arm;}
    public long getPlog_lifetime(){return this.plog_lifetime;}
    public int getConf_mag_declination(){return this.conf_mag_declination;}
    public int getConf_vbatscale(){return this.conf_vbatscale;}
    public int getConf_vbatlevel_warn1(){return this.conf_vbatlevel_warn1;}
    public int getConf_vbatlevel_warn2(){return this.conf_vbatlevel_warn2;}
    public int getConf_vbatlevel_crit(){return this.conf_vbatlevel_crit;}

    @Override
    public String getJsonStr(){
        String rtn =
                "'misc':{" +
                        "'powTri_1':'" + getIntPowerTrigger1() +
                        "','minThro':'" + getConf_minthrottle() +
                        "','maxThro':'" + getMAXTHROTTLE() +
                        "','minCmd':'" + getMINCOMMAND() +
                        "','fsThro':'" + getConf_failsafe_throttle() +
                        "','pArm':'" + getPlog_arm() +
                        "','pLifetime':'" + getPlog_lifetime() +
                        "','magDecl':'" + getConf_mag_declination() +
                        "','vbatScale':'" + getConf_vbatscale() +
                        "','vbatLv_warn1':'" + getConf_vbatlevel_warn1() +
                        "','vbatLv_warn2':'" + getConf_vbatlevel_warn1() +
                        "','vbatLv_crit':'" + getConf_vbatlevel_crit() +
                        "'}";

        return rtn;
    }

    public MspMISC(byte[] data) {
        super(MspCommand.MSP_MISC, data);

        if(data == null)
            return;

        try {
            intPowerTrigger1 = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 0, 2));
            conf_minthrottle = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 2, 4));
            MAXTHROTTLE = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 4, 6));
            MINCOMMAND = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 6, 8));
            conf_failsafe_throttle = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 8, 10));
            plog_arm = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 10, 12));
            plog_lifetime = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 12, 16));
            conf_mag_declination = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 16, 18));
            conf_vbatscale = data[18] & 0xff;
            conf_vbatlevel_warn1 = data[19] & 0xff;
            conf_vbatlevel_warn2 = data[20] & 0xff;
            conf_vbatlevel_crit = data[21] & 0xff;
        }catch (Exception e){
            throw e;
        }
    }
}
