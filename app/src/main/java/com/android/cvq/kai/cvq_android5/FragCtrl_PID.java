/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

public class FragCtrl_PID extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    
    private Button btnSend;
    
    private TextView roll_P;
    private TextView roll_I;
    private TextView roll_D;
    private TextView pitch_P;
    private TextView pitch_I;
    private TextView pitch_D;
    private TextView yaw_P;
    private TextView yaw_I;
    private TextView yaw_D;
    private TextView alt_P;
    private TextView alt_I;
    private TextView alt_D;
    private TextView pos_P;
    private TextView pos_I;
    private TextView pos_D;
    private TextView level_P;
    private TextView level_I;
    private TextView level_D;
    private TextView mag_P;
    private TextView mag_I;
    private TextView mag_D;

    private RadioGroup ctrlMode;
    private RadioButton ctrlMode_getValue;

    private StatusCheck uiUpdateLoop;
    private OnFragmentInteractionListener mListener;

    public FragCtrl_PID() {}

    // TODO: Rename and change types and number of parameters
    public static FragCtrl_PID newInstance(String param1, String param2) {
        FragCtrl_PID fragment = new FragCtrl_PID();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_ctrl__pid, container, false);

        btnSend = (Button) view.findViewById(R.id.fcPID_btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ctrlMode.getCheckedRadioButtonId() == R.id.fcPID_RadioSetup) {
                    int[] size = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

                    int[] intdata = {
                            Integer.parseInt(roll_P.getText() + ""),
                            Integer.parseInt(roll_I.getText() + ""),
                            Integer.parseInt(roll_D.getText() + ""),
                            Integer.parseInt(pitch_P.getText() + ""),
                            Integer.parseInt(pitch_I.getText() + ""),
                            Integer.parseInt(pitch_D.getText() + ""),
                            Integer.parseInt(yaw_P.getText() + ""),
                            Integer.parseInt(yaw_I.getText() + ""),
                            Integer.parseInt(yaw_D.getText() + ""),
                            Integer.parseInt(alt_P.getText() + ""),
                            Integer.parseInt(alt_I.getText() + ""),
                            Integer.parseInt(alt_D.getText() + ""),
                            Integer.parseInt(pos_P.getText() + ""),
                            Integer.parseInt(pos_I.getText() + ""),
                            Integer.parseInt(pos_D.getText() + ""),
                            CVQ_UsbSerial.getMspPID().getValuePID(MspPID.posr,MspPID.P)& 0xff,
                            CVQ_UsbSerial.getMspPID().getValuePID(MspPID.posr,MspPID.I) & 0xff,
                            CVQ_UsbSerial.getMspPID().getValuePID(MspPID.posr,MspPID.D) & 0xff,
                            CVQ_UsbSerial.getMspPID().getValuePID(MspPID.navr,MspPID.P) & 0xff,
                            CVQ_UsbSerial.getMspPID().getValuePID(MspPID.navr,MspPID.P) & 0xff,
                            CVQ_UsbSerial.getMspPID().getValuePID(MspPID.navr,MspPID.P) & 0xff,
                            Integer.parseInt(level_P.getText() + ""),
                            Integer.parseInt(level_I.getText() + ""),
                            Integer.parseInt(level_D.getText() + ""),
                            Integer.parseInt(mag_P.getText() + ""),
                            Integer.parseInt(mag_I.getText() + ""),
                            Integer.parseInt(mag_D.getText() + ""),
                            CVQ_UsbSerial.getMspPID().getValuePID(MspPID.vel,MspPID.P) & 0xff,
                            CVQ_UsbSerial.getMspPID().getValuePID(MspPID.vel,MspPID.I) & 0xff,
                            CVQ_UsbSerial.getMspPID().getValuePID(MspPID.vel,MspPID.D) & 0xff    };

                    MspSetPID mspSetPID = new MspSetPID(BytesOperation.ints2bytes(intdata, size));
                    CVQ_UsbSerial.write(SendMsp.getBytesData(mspSetPID));
                    CVQ_UsbSerial.write(SendMsp.getBytesData(new MspPID(null)));
                }
            }
        });

        CVQ_UsbSerial.write(SendMsp.getBytesData(new MspPID(null)));

        roll_P = (TextView) view.findViewById(R.id.fcPID_roll_P);        
        roll_I = (TextView) view.findViewById(R.id.fcPID_roll_I);        
        roll_D = (TextView) view.findViewById(R.id.fcPID_roll_D);
        roll_P.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, roll_P));
        roll_I.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, roll_I));
        roll_D.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, roll_D));
        
        pitch_P = (TextView) view.findViewById(R.id.fcPID_pitch_P);        
        pitch_I = (TextView) view.findViewById(R.id.fcPID_pitch_I);        
        pitch_D = (TextView) view.findViewById(R.id.fcPID_pitch_D);
        pitch_P.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, pitch_P));
        pitch_I.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, pitch_I));
        pitch_D.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, pitch_D));
        
        yaw_P = (TextView) view.findViewById(R.id.fcPID_yaw_P);
        yaw_I = (TextView) view.findViewById(R.id.fcPID_yaw_I);
        yaw_D = (TextView) view.findViewById(R.id.fcPID_yaw_D);
        yaw_P.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, yaw_P));
        yaw_I.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, yaw_I));
        yaw_D.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, yaw_D));
        
        alt_P = (TextView) view.findViewById(R.id.fcPID_alt_P);
        alt_I = (TextView) view.findViewById(R.id.fcPID_alt_I);
        alt_D = (TextView) view.findViewById(R.id.fcPID_alt_D);
        alt_P.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, alt_P));
        alt_I.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, alt_I));
        alt_D.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, alt_D));

        pos_P = (TextView) view.findViewById(R.id.fcPID_pos_P);
        pos_I = (TextView) view.findViewById(R.id.fcPID_pos_I);
        pos_D = (TextView) view.findViewById(R.id.fcPID_pos_D);
        pos_P.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, pos_P));
        pos_I.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, pos_I));
        pos_D.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, pos_D));
        
        level_P = (TextView) view.findViewById(R.id.fcPID_level_P);
        level_I = (TextView) view.findViewById(R.id.fcPID_level_I);
        level_D = (TextView) view.findViewById(R.id.fcPID_level_D);
        level_P.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, level_P));
        level_I.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, level_I));
        level_D.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, level_D));
        
        mag_P = (TextView) view.findViewById(R.id.fcPID_mag_P);
        mag_I = (TextView) view.findViewById(R.id.fcPID_mag_I);
        mag_D = (TextView) view.findViewById(R.id.fcPID_mag_D);
        mag_P.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, mag_P));
        mag_I.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, mag_I));
        mag_D.setOnTouchListener(new OnTouch2SeekBarListener(null, 0, 255, mag_D));

        ctrlMode = (RadioGroup) view.findViewById(R.id.fcPID_CtrlMode);
        ctrlMode_getValue = (RadioButton) view.findViewById(R.id.fcPID_RadioValue);
        ctrlMode_getValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspPID(null)));
            }
        });

        return view;
    }

    @Override
    public void onPause(){
        super.onPause();
        uiUpdateLoop.stop();
        uiUpdateLoop = null;
    }

    @Override
    public void onResume(){
        super.onResume();
        uiUpdateLoop = new StatusCheck();
        uiUpdateLoop.start();
        uiUpdateLoop.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }

    private class StatusCheck extends CVQ_Async{

        @Override
        void updateValue() {
            MspPID mspPID = CVQ_UsbSerial.getMspPID();
            try {
                if (ctrlMode.getCheckedRadioButtonId() == R.id.fcPID_RadioValue) {
                    btnSend.setClickable(false);
                    btnSend.setAlpha((float) 0.5);
                    if (mspPID != null) {
                        roll_P.setText((mspPID.getValuePID(MspPID.roll,MspPID.P) & 0xff) + "");
                        roll_I.setText((mspPID.getValuePID(MspPID.roll,MspPID.I) & 0xff) + "");
                        roll_D.setText((mspPID.getValuePID(MspPID.roll,MspPID.D) & 0xff) + "");
                        pitch_P.setText((mspPID.getValuePID(MspPID.pitch,MspPID.P) & 0xff) + "");
                        pitch_I.setText((mspPID.getValuePID(MspPID.pitch,MspPID.I) & 0xff) + "");
                        pitch_D.setText((mspPID.getValuePID(MspPID.pitch,MspPID.D) & 0xff) + "");
                        yaw_P.setText((mspPID.getValuePID(MspPID.yaw,MspPID.P) & 0xff) + "");
                        yaw_I.setText((mspPID.getValuePID(MspPID.yaw,MspPID.I) & 0xff) + "");
                        yaw_D.setText((mspPID.getValuePID(MspPID.yaw,MspPID.D) & 0xff) + "");
                        alt_P.setText((mspPID.getValuePID(MspPID.alt,MspPID.P) & 0xff) + "");
                        alt_I.setText((mspPID.getValuePID(MspPID.alt,MspPID.I) & 0xff) + "");
                        alt_D.setText((mspPID.getValuePID(MspPID.alt,MspPID.D) & 0xff) + "");
                        pos_P.setText((mspPID.getValuePID(MspPID.pos,MspPID.P) & 0xff) + "");
                        pos_I.setText((mspPID.getValuePID(MspPID.pos,MspPID.I) & 0xff) + "");
                        pos_D.setText((mspPID.getValuePID(MspPID.pos,MspPID.D) & 0xff) + "");
                        level_P.setText((mspPID.getValuePID(MspPID.level,MspPID.P) & 0xff) + "");
                        level_I.setText((mspPID.getValuePID(MspPID.level,MspPID.I) & 0xff) + "");
                        level_D.setText((mspPID.getValuePID(MspPID.level,MspPID.D) & 0xff) + "");
                        mag_P.setText((mspPID.getValuePID(MspPID.mag,MspPID.P) & 0xff) + "");
                        mag_I.setText((mspPID.getValuePID(MspPID.mag,MspPID.I) & 0xff) + "");
                        mag_D.setText((mspPID.getValuePID(MspPID.mag,MspPID.D) & 0xff) + "");
                    }
                    CVQ_UsbSerial.write(SendMsp.getBytesData(new MspPID(null)));
                } else {
                    btnSend.setClickable(true);
                    btnSend.setAlpha((float) 1);
                }
            }catch (Exception e){
                ErrorCatcher.addErrMsg("[" + LogCatcher._FUNC_() + "]: " + e.toString());
            }
        }
    }
}
