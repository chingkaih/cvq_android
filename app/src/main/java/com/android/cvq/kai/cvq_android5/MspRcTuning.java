/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

public class MspRcTuning extends AbstractMsp {
    private int byteRC_RATE         = -1;   // uint 8
    private int byteRC_EXPO         = -1;   // uint 8
    private int byteRollPitchRate   = -1;   // uint 8
    private int byteYawRate         = -1;   // uint 8
    private int byteDynThrPID       = -1;   // uint 8
    private int byteThrottle_MID    = -1;   // uint 8
    private int byteThrottle_EXPO   = -1;   // uint 8

    public MspRcTuning(byte[] data) {
        super(MspCommand.MSP_RC_TUNING, data);

        if(data == null){
            return;
        }

        for(byte b : data){
            int tmp = (int) b & 0xff;
            if(tmp < 0 || tmp > 100){
                ErrorCatcher.addErrMsg("[MspRcTuning]: Attribute not in range (0~100).");
            }
        }

        try {
            this.byteRC_RATE = (int) data[0] & 0xff;
            this.byteRC_EXPO = (int) data[1] & 0xff;
            this.byteRollPitchRate = (int) data[2] & 0xff;
            this.byteYawRate = (int) data[3] & 0xff;
            this.byteDynThrPID = (int) data[4] & 0xff;
            this.byteThrottle_MID = (int) data[5] & 0xff;
            this.byteThrottle_EXPO = (int) data[6] & 0xff;
        }catch (Exception e){
            throw e;
        }
    }

    @Override
    public String getJsonStr(){
        String rtn =
                "'rc_tuning':{" +
                        "'rate':'" + getByteRC_RATE() +
                        "','expo':'" + getByteRC_EXPO() +
                        "','rollpitch_rate':'" + getByteRollPitchRate() +
                        "','yaw_rate':'" + getByteYawRate() +
                        "','dynthr_pid':'" + getByteDynThrPID() +
                        "','thro_mid':'" + getByteThrottle_MID() +
                        "','thro_expo':'" + getByteThrottle_EXPO() +
                        "'}";

        return rtn;
    }

    public int getByteRC_RATE(){
        return this.byteRC_RATE;
    }

    public int getByteRC_EXPO(){
        return this.byteRC_EXPO;
    }

    public int getByteRollPitchRate(){
        return this.byteRollPitchRate;
    }

    public int getByteYawRate(){
        return this.byteYawRate;
    }

    public int getByteDynThrPID(){
        return this.byteDynThrPID;
    }

    public int getByteThrottle_MID(){
        return this.byteThrottle_MID;
    }

    public int getByteThrottle_EXPO(){
        return this.byteThrottle_EXPO;
    }
}
