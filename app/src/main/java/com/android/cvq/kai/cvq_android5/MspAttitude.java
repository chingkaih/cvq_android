/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import java.util.Arrays;

public class MspAttitude extends AbstractMsp {
    private int angX;   // int 16, -1800~1800
    private int angY;   // int 16,  -900~900
    private int heading;    // int 16, -180~180

    public int getAngX(){return this.angX;}
    public int getAngY(){return this.angY;}
    public int getHeading(){return this.heading;}

    public MspAttitude(byte[] data) {
        super(MspCommand.MSP_ATTITUDE, data);

        try {
            this.angX = BytesOperation.bytes2int(Arrays.copyOfRange(data, 0, 2));
            this.angY = BytesOperation.bytes2int(Arrays.copyOfRange(data, 2, 4));
            this.heading = BytesOperation.bytes2int(Arrays.copyOfRange(data, 4, 6));
        }catch(Exception e){
            throw e;
        }
    }

    @Override
    public String getJsonStr() {
        String rtn =
                "'attitude':{" +
                        "'angx':'" + angX +
                        "','angy':'" + angY +
                        "','heading':'" + heading +
                        "'}";

        return rtn;
    }
}
