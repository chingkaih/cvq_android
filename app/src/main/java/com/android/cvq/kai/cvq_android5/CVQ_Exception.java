package com.android.cvq.kai.cvq_android5;

/**
 * Created by andrew on 11/20/15.
 */
public class CVQ_Exception extends Exception {
    private String errMsg;

    public CVQ_Exception(String errMsg){
        this.errMsg = errMsg;
    }

    public String getErrMsg(){
        return this.errMsg;
    }
}
