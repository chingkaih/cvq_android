/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import java.util.Arrays;

public class MspServo extends AbstractMsp {
    private int[] servo;

    public MspServo(byte[] data){
        super(MspCommand.MSP_SERVO,data);

        int y = 0;
        if(data == null){
            for (int x = 0; x < 16; x++) {
                this.servo[x] = -1;
                y = y + 2;
            }
            return;
        }

        try {
            for (int x = 0; x < 16; x++) {
                this.servo[x] = BytesOperation.bytes2uint(Arrays.copyOfRange(data, y, y + 2));
                y = y + 2;
            }
        }catch(Exception e){
            throw e;
        }
    }

    @Override
    public String getJsonStr(){
        String rtn = "'servo':{";
        for(int x=0; x<16; x++){
            rtn += "'servo_" + x + "':'" + getServo(x) + "',";
        }
        rtn += "\b}";

        return rtn;
    }

    public int getServo(int index){
        if(index < 0 || index >= 16){
            return -1;
        }
        return this.servo[index];
    }
}
