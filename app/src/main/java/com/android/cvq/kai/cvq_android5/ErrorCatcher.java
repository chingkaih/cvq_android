/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

public class ErrorCatcher extends MsgCatcher{
    private static boolean getError = false;
    private static String errMsg = "";

    public static void addErrMsg(String msg){
        errMsg = errMsg + "\n" + currentDate() + msg;
        getError = true;
    }

    public static String getErrMsg(){
        String rtn = errMsg;
        return rtn;
    }

    public static void clearErrMsg(){
        errMsg = "";
        getError = false;
    }

    public static boolean isGetErr(){
        return getError;
    }
}
