/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import java.util.Arrays;

public class MspAltitude extends AbstractMsp {
    private int estAlt; // int 32
    private int vario;  // int 16

    public int getEstAlt(){return this.estAlt;}
    public int getVario(){return this.vario;}

    public MspAltitude(byte[] data) {
        super(MspCommand.MSP_ALTITUDE, data);

        try {
            this.estAlt = BytesOperation.bytes2int(Arrays.copyOfRange(data, 0, 4));
            this.vario = BytesOperation.bytes2int(Arrays.copyOfRange(data, 4, 6));
        }catch (Exception e){
            throw e;
        }
    }

    @Override
    public String getJsonStr() {
        String rtn =
                "'altitude':{" +
                        "'estalt':'" + estAlt +
                        "','vario':'" + vario +
                        "'}";

        return rtn;
    }
}
