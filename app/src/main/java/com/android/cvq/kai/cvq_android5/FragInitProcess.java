package com.android.cvq.kai.cvq_android5;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
//import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

public class FragInitProcess extends Fragment {
    public final static int maxProgress = 100;
    public final static int minProgress = 0;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private ProgressBar initProgress;
    private TextView initText;
    private InitProgressUpdater uiUpdate;
    private int progressValue = minProgress;
    private String progressText = "Initialization";

    public static FragInitProcess newInstance(String param1, String param2) {
        FragInitProcess fragment = new FragInitProcess();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragInitProcess() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_init_process, container, false);
        initProgress = (ProgressBar) view.findViewById(R.id.progressBar);
        initText = (TextView) view.findViewById(R.id.initText);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setInitText(String msg) {
        uiUpdate = new InitProgressUpdater();
        progressText = msg;
        uiUpdate.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
    }

    public void setInitProgress(int progress) {
        uiUpdate = new InitProgressUpdater();
        if (progress > maxProgress) {
            progressValue = maxProgress;
        } else if (progress < minProgress) {
            progressValue = minProgress;
        } else {
            progressValue = progress;
        }
        uiUpdate.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
    }

    public interface OnFragmentInteractionListener {
    }

    private class InitProgressUpdater extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            publishProgress();
            return null;
        }

        @Override
        protected void onProgressUpdate(Object[] obj) {
            initText.setText(progressText);
            initProgress.setProgress(progressValue);
        }
    }

    ;
}
