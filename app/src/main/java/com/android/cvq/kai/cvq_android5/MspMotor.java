/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import java.util.Arrays;

public class MspMotor extends AbstractMsp {
    // Motor:   3, 5, 6, 2
    // Array:   0, 1, 2, 3
    private int[] motor = new int[8];

    public MspMotor(byte[] data){
        super(MspCommand.MSP_MOTOR,data);

        int y = 0;
        if(data == null){
            for (int x = 0; x < 8; x++) {
                this.motor[x] = -1;
                y = y + 2;
            }
            return;
        }

        try {
            for (int x = 0; x < 8; x++) {
                this.motor[x] = BytesOperation.bytes2uint(Arrays.copyOfRange(data, y, y + 2));
                y = y + 2;
            }
        }catch(Exception e){
            throw e;
        }
    }

    @Override
    public String getJsonStr(){
        String rtn =
                "'motor':{" +
                        "'m2':'" + getMotor(3) +
                        "','m5':'" + getMotor(1) +
                        "','m6':'" + getMotor(2) +
                        "','m3':'" + getMotor(0) +
                        "'}";

        return rtn;
    }

    public int getMotor(int index){
        if(index < 0 || index >= 8){
            return -1;
        }
        return this.motor[index];
    }
}
