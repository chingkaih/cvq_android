/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

public class FragCtrl_RcTuning extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private SeekBar valueBar;
    private Button btnSend;

    private TextView rcRate;
    private TextView rcExpo;
    private TextView rollPitchRate;
    private TextView yawRate;
    private TextView dynThrPID;
    private TextView throtMid;
    private TextView throtExpo;

    private RadioGroup ctrlMode;
    private RadioButton ctrlMode_getValue;
    private RadioButton ctrlMode_setup;

    private OnFragmentInteractionListener mListener;
    private StatusCheck uiUpdateLoop;

    public FragCtrl_RcTuning() {}

    public static FragCtrl_RcTuning newInstance(String param1, String param2) {
        FragCtrl_RcTuning fragment = new FragCtrl_RcTuning();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_ctrl__rc_tuning, container, false);

        valueBar = (SeekBar) view.findViewById(R.id.fcRcTuning_ValueBar);

        rcRate = (TextView) view.findViewById(R.id.fcRcTuning_RcRate);
        rcRate.setOnTouchListener(new OnTouch2SeekBarListener(valueBar, 0, 100, rcRate));

        rcExpo = (TextView) view.findViewById(R.id.fcRcTuning_RcExpo);
        rcExpo.setOnTouchListener(new OnTouch2SeekBarListener(valueBar, 0, 100, rcExpo));

        rollPitchRate = (TextView) view.findViewById(R.id.fcRcTuning_RollPitchRate);
        rollPitchRate.setOnTouchListener(new OnTouch2SeekBarListener(valueBar, 0, 100, rollPitchRate));

        yawRate = (TextView) view.findViewById(R.id.fcRcTuning_YawRate);
        yawRate.setOnTouchListener(new OnTouch2SeekBarListener(valueBar, 0, 100, yawRate));

        dynThrPID = (TextView) view.findViewById(R.id.fcRcTuning_DynThrPID);
        dynThrPID.setOnTouchListener(new OnTouch2SeekBarListener(valueBar, 0, 100, dynThrPID));

        throtMid = (TextView) view.findViewById(R.id.fcRcTuning_ThroMid);
        throtMid.setOnTouchListener(new OnTouch2SeekBarListener(valueBar, 0, 100, throtMid));

        throtExpo = (TextView) view.findViewById(R.id.fcRcTuning_ThroExpo);
        throtExpo.setOnTouchListener(new OnTouch2SeekBarListener(valueBar, 0, 100, throtExpo));

        ctrlMode = (RadioGroup) view.findViewById(R.id.fcRcTuning_CtrlMode);
        ctrlMode_getValue = (RadioButton) view.findViewById(R.id.fcRcTuning_RadioValue);
        ctrlMode_getValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSend.setClickable(false);
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRcTuning(null)));
            }
        });
        ctrlMode_setup = (RadioButton) view.findViewById(R.id.fcRcTuning_RadioValue);
        ctrlMode_setup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSend.setClickable(true);
            }
        });

        btnSend = (Button) view.findViewById(R.id.fcRcTuning_BtnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ctrlMode.getCheckedRadioButtonId() == R.id.fcRcTuning_RadioSetup) {
                    int[] size = {1, 1, 1, 1, 1, 1, 1};

                    try {
                        int[] intdata = {
                                Integer.parseInt(rcRate.getText() + ""),
                                Integer.parseInt(rcExpo.getText() + ""),
                                Integer.parseInt(rollPitchRate.getText() + ""),
                                Integer.parseInt(yawRate.getText() + ""),
                                Integer.parseInt(dynThrPID.getText() + ""),
                                Integer.parseInt(throtMid.getText() + ""),
                                Integer.parseInt(throtExpo.getText() + "")};

                        MspSetRcTuning mspSetRcTuning = new MspSetRcTuning(BytesOperation.ints2bytes(intdata, size));
                        CVQ_UsbSerial.write(SendMsp.getBytesData(mspSetRcTuning));
                        CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRcTuning(null)));
                    } catch (Exception e) {
                        ErrorCatcher.addErrMsg("[FragCtrl_RcTuning]: " + e.getMessage());
                    }
                }
            }
        });

        CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRcTuning(null)));

        return view;
    }

    @Override
    public void onPause(){
        super.onPause();
        uiUpdateLoop.stop();
        uiUpdateLoop = null;
    }

    @Override
    public void onResume(){
        super.onResume();
        uiUpdateLoop = new StatusCheck();
        uiUpdateLoop.start();
        uiUpdateLoop.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }

    private class StatusCheck extends CVQ_Async{

        @Override
        void updateValue() {
            MspRcTuning mspRcTuning = CVQ_UsbSerial.getMspRcTuning();
            if(ctrlMode.getCheckedRadioButtonId() == R.id.fcRcTuning_RadioValue) {
                if (mspRcTuning != null) {
                    rcRate.setText(mspRcTuning.getByteRC_RATE() + "");
                    rcExpo.setText(mspRcTuning.getByteRC_EXPO() + "");
                    rollPitchRate.setText(mspRcTuning.getByteRollPitchRate() + "");
                    yawRate.setText(mspRcTuning.getByteYawRate() + "");
                    dynThrPID.setText(mspRcTuning.getByteDynThrPID() + "");
                    throtMid.setText(mspRcTuning.getByteThrottle_MID() + "");
                    throtExpo.setText(mspRcTuning.getByteThrottle_EXPO() + "");
                }
                CVQ_UsbSerial.write(SendMsp.getBytesData(new MspRcTuning(null)));
            }
        }
    }
}
