/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import java.util.Arrays;

public class MspRC extends AbstractMsp {
    private int rcRoll;
    private int rcPitch;
    private int rcYaw;
    private int rcThrottle;

    public MspRC(byte[] data){
        super(MspCommand.MSP_RC,data);

        if(data == null){
            this.rcRoll = this.rcPitch = this.rcYaw = this.rcThrottle = -1;
            return;
        }

        try {
            this.rcRoll = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 0, 2));
            this.rcPitch = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 2, 4));
            this.rcYaw = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 4, 6));
            this.rcThrottle = BytesOperation.bytes2uint(Arrays.copyOfRange(data, 6, 8));
        }catch(Exception e){
            throw e;
        }
    }

    @Override
    public String getJsonStr(){
        String rtn =
                "'rc':{" +
                        "'roll':'" + getRcRoll() +
                        "','pitch':'" + getRcPitch() +
                        "','yaw':'" + getRcYaw() +
                        "','throttle':'" + getRcThrottle() +
                        "'}";

        return rtn;
    }

    public int getRcRoll(){return this.rcRoll;}
    public  int getRcPitch() {return this.rcPitch;}
    public int getRcYaw(){return this.rcYaw;}
    public int getRcThrottle(){return this.rcThrottle;}
}
