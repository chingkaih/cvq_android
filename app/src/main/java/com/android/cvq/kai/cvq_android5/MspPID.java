/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import java.util.HashMap;
import java.util.Map;

public class MspPID extends AbstractMsp {
    public String message;

    // ROLL / PITCH / YAW / ALT / POS / POSR / NAVR / LEVEL /MAG / VEL
    private Map<String, ClassPID> pidSet = new HashMap();
    public static final int P = 0, I = 1, D = 2;

    public static final String
            roll    = "roll",
            pitch   = "pitch",
            yaw     = "yaw",
            alt     = "alt",
            pos     = "pos",
            posr    = "posr",
            navr    = "navr",
            level   = "level",
            mag     = "mag",
            vel     = "vel";

    public static final String[] pidItems = {roll, pitch, yaw, alt, pos, posr, navr, level, mag, vel};

    public MspPID(byte[] data) {
        super(MspCommand.MSP_PID, data);

        if(data == null)
            return;

        int index = 0;
        try{
            for(int x=0; x<pidItems.length; x++) {
                pidSet.put(pidItems[x], new ClassPID(data[index] & 0xff, data[index + 1] & 0xff, data[index + 2] & 0xff));
                index += 3;
            }
        }catch(Exception e){
            throw e;
        }
    }

    public int getValuePID(String key, int pidId){
        switch (pidId) {
            case P:
                return pidSet.get(key).p;
            case I:
                return pidSet.get(key).i;
            case D:
                return pidSet.get(key).d;
        }
        return -1;
    }

    @Override
    public String getJsonStr() {
        // ROLL / PITCH / YAW / ALT / POS / POSR / NAVR / LEVEL /MAG / VEL
        String rtn = "'pid':{";
        try {
            for (int x = 0; x < 10; x++) {
                String tmp = pidSet.get(pidItems[x]).getJsonStr();

                rtn += "'" + pidItems[x] + "':" + tmp;
                if (pidItems[x] != pidItems[pidItems.length - 1])
                    rtn += ",";
            }
        }catch(Exception e){
            throw e;
        }
        rtn += "}";

        return rtn;
    }
}
