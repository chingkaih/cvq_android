/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import java.util.LinkedList;
import java.util.List;

public class SendMsp {

    public static byte[] getBytesData(AbstractMsp abstractMsp) {
        return bytesRequestMSP(requestMSP(abstractMsp.getId(), abstractMsp.getDataBytes()));
    }

    //-----------------------------------------
    public static List<Byte> requestMSP(int msp) {
        return requestMSP(msp, null);
    }

    public static byte[] cmdIds2bytes(int[] msps) {
        List<Byte> s = new LinkedList<Byte>();
        for (int m : msps) {
            s.addAll(requestMSP(m, null));
        }
        return bytesRequestMSP(s);
    }

    public static List<Byte> requestMsp(int[] mspIdList) {
        List<Byte> s = new LinkedList<Byte>();
        for (int mspId : mspIdList) {
            s.addAll(requestMSP(mspId, null));
        }

        return s;
    }

    public static List<Byte> requestMSP(int msp, byte[] payload) {
        if (msp < 0) {
            return null;
        }
        List<Byte> bf = new LinkedList<Byte>();
        for (char c : MspCommand.mspHeader) {
            bf.add((byte) c);
        }
        bf.add((byte) '<');

        byte checksum = 0;
        byte pl_size = (byte) ((payload != null ? (int) (payload.length) : 0) & 0xFF);
        bf.add(pl_size);
        checksum ^= (pl_size & 0xFF);

        bf.add((byte) (msp & 0xFF));
        checksum ^= (msp & 0xFF);

        if (payload != null) {
            for (byte c : payload) {
                bf.add((byte) (c & 0xFF));
                checksum ^= (c & 0xFF);
            }
        }
        bf.add(checksum);
        return (bf);
    }

    public static byte[] bytesRequestMSP(List<Byte> msp) {
        byte[] arr = new byte[msp.size()];
        int i = 0;
        for (byte b : msp) {
            arr[i++] = b;
        }
        return arr;
    }
}
