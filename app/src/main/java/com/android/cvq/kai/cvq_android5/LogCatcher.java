/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;

public class LogCatcher extends MsgCatcher {
    private static String logMsg = "";

    private static void readLog(String path){
        try{
            BufferedReader logFile = new BufferedReader(new FileReader(path));
            String line = "";
            do{
                addLogMsg(line);
                line = logFile.readLine();
            }while(line != null);

        }catch(Exception e){
            ErrorCatcher.addErrMsg("[LogCarcher]: " + e.getMessage());
        }
    }

    public static void writeLog(String path){
        addLogMsg("Writing log into file.");
        try {
            PrintWriter writer = new PrintWriter(path, "UTF-8");
            writer.print(logMsg);
            writer.close();
        } catch (Exception e) {
            ErrorCatcher.addErrMsg("[LogCreater]: " + e.getMessage());
        }

        clearLog();
        addLogMsg("Log writed into file, " + path + ".");
    }

    public static void addLogMsg(String msg){
        logMsg = logMsg + "\n" + currentDate() + msg;
    }

    public static void clearLog(){
        logMsg = "";
    }

    public static String getLogMsg(){
        return logMsg;
    }
}
