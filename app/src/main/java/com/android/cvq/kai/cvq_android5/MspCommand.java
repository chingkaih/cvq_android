/* Copyright 2015 Ching-Kai Huang <ChingKai.Huang001@umb.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * Project home page: https://bitbucket.org/chingkaih/cvq_android/src
 */

package com.android.cvq.kai.cvq_android5;

import java.util.Arrays;

public class MspCommand {
    public static final int
            MSP_IDENT = 100,
            MSP_STATUS = 101,
            MSP_RAW_IMU = 102,
            MSP_SERVO = 103,
            MSP_MOTOR = 104,
            MSP_RC = 105,
            MSP_RAW_GPS = 106,
            MSP_COMP_GPS = 107,
            MSP_ATTITUDE = 108,
            MSP_ALTITUDE = 109,
            MSP_ANALOG = 110,
            MSP_RC_TUNING = 111,
            MSP_PID = 112,
            MSP_BOX = 113,
            MSP_MISC = 114,
            MSP_MOTOR_PINS = 115,
            MSP_BOXNAMES = 116,
            MSP_PIDNAMES = 117,
            MSP_SERVO_CONF = 120,
            MSP_SET_RAW_RC = 200,
            MSP_SET_RAW_GPS = 201,
            MSP_SET_PID = 202,
            MSP_SET_BOX = 203,
            MSP_SET_RC_TUNING = 204,
            MSP_ACC_CALIBRATION = 205,
            MSP_MAG_CALIBRATION = 206,
            MSP_SET_MISC = 207,
            MSP_RESET_CONF = 208,
            MSP_SELECT_SETTING = 210,
            MSP_SET_HEAD = 211, // Not used
            MSP_SET_SERVO_CONF = 212,
            MSP_SET_MOTOR = 214,
            MSP_BIND = 241,
            MSP_EEPROM_WRITE = 250,
            MSP_DEBUGMSG = 253,
            MSP_DEBUG = 254;


    public static final char[] mspHeader = {'$', 'M'};
    public static final char failType = 'X';
    private char cmdType;
    private String errMsg = "";
    private AbstractMsp classMsp = null;

    public MspCommand(byte[] allCmdBytes) {
        if ((allCmdBytes.length < 5) || (allCmdBytes.length != (allCmdBytes[3] & 0xff) + 6)) {
            this.cmdType = failType;
            errMsg = "Command too short. (size=" + allCmdBytes.length + "," + ((allCmdBytes[3] & 0xff) + 6) + ")";
            return;
        } else {
            for (int x = 0; x < mspHeader.length-1; x++) {
                if (allCmdBytes[x] != mspHeader[x]) {
                    this.cmdType = failType;
                    errMsg = "Unfited header.";
                    return;
                }
            }
        }

        this.cmdType = (char) allCmdBytes[2];

        switch (this.cmdType) {
            case '>':
                try {
                    this.classMsp = cmdAnalysis(allCmdBytes[4], Arrays.copyOfRange(allCmdBytes, 5, allCmdBytes.length));
                }catch (Exception e){
                    ErrorCatcher.addErrMsg(e.getMessage());
                    this.classMsp = null;
                }
                break;
            case '<':
                errMsg = "Unused '<' type command.";
                break;
            case '!':
                break;
            default:
                errMsg = "Unknown command type. (" + this.cmdType + ")";
                break;
        }
    }

    private AbstractMsp cmdAnalysis(int cmdNum, byte[] dataBytes) {
        switch (cmdNum) {
            case MSP_IDENT:
                return new MspIdent(dataBytes);
            case MSP_STATUS:
                return new MspStatus(dataBytes);
            case MSP_RAW_IMU:
                return new MspRawImu(dataBytes);
            case MSP_SERVO:
                return new MspServo(dataBytes);
            case MSP_MOTOR:
                return new MspMotor(dataBytes);
            case MSP_PID:
                return new MspPID(dataBytes);
            case MSP_RC:
                return new MspRC(dataBytes);
            case MSP_RC_TUNING:
                return new MspRcTuning(dataBytes);
            case MSP_MISC:
                return new MspMISC(dataBytes);
            case MSP_ATTITUDE:
                return new MspAttitude(dataBytes);
            case MSP_ALTITUDE:
                return new MspAltitude(dataBytes);
        }
        return null;
    }

    public String   getErrMsg() {
        return this.errMsg;
    }
    public char     getCmdType() {
        return this.cmdType;
    }
    public AbstractMsp getAbstractMsp() { return this.classMsp;}

}
