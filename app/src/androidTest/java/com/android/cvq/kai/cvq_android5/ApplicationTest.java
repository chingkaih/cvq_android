package com.android.cvq.kai.cvq_android5;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;

import java.util.Arrays;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

//    @SmallTest
//    public void test_MspIdent_getCapability() throws Exception {
//        super.setUp();
//        byte[] bArray = {0b00000000, 0b00000000, 0b00000000, 0b00000001, 0b00000011, 0b00000111, 0b00001111};
//        MspIdent aaa = new MspIdent(bArray);
//        assertEquals(0b1000000110000011100001111, aaa.getCapability());
//    }
//
//    @SmallTest
//    public void test2() throws Exception{
//        byte[] data = {'$', 'M', '>', 7, 100, 1, 1, 1, 1, 1, 1, 1, 111};
//        MspCommand mmm = new MspCommand(data);
//        assertEquals(mmm.getErrMsg(), "");
//        assertEquals(mmm.getCmdType(), '>');
//
////        byte[] tmp = {1,1,1,1,1,1,1};
////        assertSame(mmm.getCmdObject().getClass(), (new MspIdent(Arrays.copyOfRange(data, 5, data.length - 1), data[4]&0xff)).getClass());
////        assertEquals(mmm.getCmdObject().getClass(), null);
//    }
//
////    @SmallTest
////    public void test3() throws Exception{
////        byte[] tmp1 = {1,1,1};
////        //assertEquals(Integer.toBinaryString(BytesOperation.bytes2uint(tmp1)), "11111111111111110000000100000001");
//////        assertEquals(Integer.toBinaryString(65793), "");
//////        assertEquals(Integer.toBinaryString(BytesOperation.bytes2int(tmp1)), "");
////        assertEquals(BytesOperation.bytes2int(tmp1), 65793);
////        byte[] tmp2 = {1,1,1,1};
////        assertEquals(BytesOperation.bytes2int(tmp2), 16843009);
////        byte[] tmp3 = {1};
////        assertEquals(BytesOperation.bytes2int(tmp3), 1);
////    }
//
//    @SmallTest
//    public void test_int2bytes(){
//        int i = 123;
//        byte[] result = {123,0,0,0};
//        byte[] test = BytesOperation.int2bytes(i);
//        assertEquals(result.length, test.length);
//        for (int x = 0; x < test.length; x++) {
//            assertEquals(result[x], test[x]);
//        }
//    }
//
//    @SmallTest
//    public void test_ints2bytes() throws Exception {
//        int[] data = {123, 234, 300, 40};
//        int[] size = {1, 2, 3, 4};
//        byte[] result = {123, (byte) 234, 0, 44, 1, 0, 40, 0, 0, 0};
//
//        byte[] sss = BytesOperation.ints2bytes(data, size);
//
//        for (int x = 0; x < sss.length; x++) {
//            assertEquals(result[x], sss[x]);
//        }
//    }

    @SmallTest
    public void test_simpleByteOpt() throws Exception {
//        assertEquals(255, 0b11111111);
//        assertEquals((byte) 127, 0b01111111);
//        assertEquals((byte) -1, (byte) 0b11111111);
//        assertEquals((byte) 127,  (byte) 0b01111111);
//        assertEquals((byte) -127, (byte) 0b10000001);
//        assertEquals(0b10000000, 0b01111111^0b11111111);
//        assertEquals((byte) 0b10000000, (byte) (0b01111111^0b11111111));
//        assertEquals((byte) -127, (byte) ((0b01111111^0b11111111)|0b00000001));
//        assertEquals((byte) -127, ((byte)0b01111111^(byte)0b11111111)|(byte)0b00000001);
//
//        assertEquals(((byte)-22) &0xff, -22&0xff);
//
//        assertEquals((int)((byte) -123), -123);
//        assertEquals((byte) -1, (byte)255);
//
//        assertEquals(Integer.toBinaryString(255), "11111111");
//
//        byte[] data = {0,1,2,3,4,5,6,7,8,9};
//        byte[] sss = Arrays.copyOfRange(data, 3, 8);
//        byte[] result = {3,4,5,6,7};
//        for (int x = 0; x < sss.length; x++) {
//            assertEquals(result[x], sss[x]);
//        }

//        byte[] ddd = {6,0};
//
//        byte[] aaa = Arrays.copyOfRange(ddd,0,2);
//        assertEquals(BytesOperation.bytes2int(Arrays.copyOfRange(ddd,0,2)),6);

//        assertEquals(Integer.toBinaryString(49152), Integer.toBinaryString(-114688));
//        assertEquals(BytesOperation.b16int_us2s(49152), 0);
    }
}