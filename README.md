# CVQ_Android v.0.6.2.0   (Nov.30th, 2015)
!!! This project is still in its developing period. !!!

![CVQ_schematic_diagram.jpeg](https://bitbucket.org/repo/xqRq4x/images/2935096313-CVQ_schematic_diagram.jpeg)

# Quick summary
==> Last Update: Nov.30th, 2015       Ching-Kai Huang

This Android application is one part of project CV_Quadcopter. It builds the connection between the drone(Arduino) and a cloud server(to talk to remoter side). 

@ Socket.IO to talk with cloud server.
  (http://socket.io/)

@ Usb-Serial-for-Android, to build the connection of Arduino. 
  (https://github.com/mik3y/usb-serial-for-android) 

@ The controlling of drone is base on MultiWii(2.4) Serial Protocol.
  (http://www.multiwii.com/)


[ Note ]---------

@ Those code in this repository is the 2nd generation of CVQ_Android. The first one was trying to make control to MultiWii by direct hack in it's code without go through MSP, which been proved is a bad way to go. In this code(Nov.17th, 2015), the MSP been implemented, and also the interface also improved by multiple fragments.

[To Do List]--------

@ Redefine and implement the command script structure between remoter, server, client, MSP.

@ Add GPS coordinate.

[Developing Environment]--------

@ Compiling Env: Linux 3.13.0-68-generic, Ubuntu 14.04, Android Studio 1.4.1, JRE 1.8.0_45-b14 amd64

@ Android Smart Phones: Nexus 7(Android 6.0), Motor G(Android 4.4)

==========================================================================
# Author, License, and Copyright
CVQ_Android is written and maintain by Ching-Kai Huang
Those codes are licensed under LGPL Version 2.1. Please see LICENSE.txt for the complete license. For module 'usb-serial-for-android' and 'socket.io'; please read their official website for up-to-date license declaration.

@ usb-serial-for-android: (LGPL v 2.1)
    https://github.com/mik3y/usb-serial-for-android

@ socket.io: (MIT)
    https://github.com/socketio/socket.io
==========================================================================
# About Author
   [Ching-Kai Huang : ChingKai.Huang001@umb.edu](ChingKai.Huang001@umb.edu)

There still lots of thing I need to learn. 
Your suggestion and opinion are more than welcome!

==========================================================================
# About Project, CV_Quadcopter

==> Last Update: Dec. 1st, 2015       Ching-Kai Huang

It started at June, 2015 as an independent project of Ching-Kai Huang. The goal of this project is to build a far end remotable drone, that carry an Android smart phone as a message send/receiver and a data pre-analyser. This system connects a drone(), Android smart phone, cloud server(Django), and remoter(Web page or python interface(Tkinter)).

[Drafting Plan](https://drive.google.com/file/d/0B6J-WQ1mCbg8SUxxall3VG1xN1E/view?usp=sharing)

[Socket.IO Structure v.0.5(Discrad)](https://drive.google.com/open?id=0B6J-WQ1mCbg8NlhMeWRBanJwZVk)

[Command Structure v.0.5(Discrad)](https://drive.google.com/file/d/0B6J-WQ1mCbg8UDlnMnMyblVRdWs/view?usp=sharing)

![CVQ_1.jpg](https://bitbucket.org/repo/xqRq4x/images/1693429119-CVQ_1.jpg)

--------------------------------------------------------------------------